package lt.akademija.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 *
 * Customizes basic Spring Security features.
 *
 */
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired //  lt.akademija.service.AccountService
	public UserDetailsService userDetailsService;

	@Autowired // Bean in App.java
	private PasswordEncoder passwordEncoder;

	/**
	 * Method obtaining authentication manager which in turn processes
	 * Authentication requests.
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	/**
	 * Method exposing the AuthenticationManager from
	 * configure(AuthenticationManagerBuilder) to be exposed as a Bean.
	 */
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

//	@Bean
//	CorsConfigurationSource corsConfigurationSource() {
//		CorsConfiguration configuration = new CorsConfiguration();
//		configuration.setAllowedOrigins(Arrays.asList("*"));
//        configuration.setAllowedMethods(Arrays.asList("*"));
//        configuration.setAllowedHeaders(Arrays.asList("*"));
//		configuration.setAllowCredentials(true);
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		source.registerCorsConfiguration("/**", configuration);
//		return source;
//	}

	/**
	 * Method defining the behaviour of global and path-specific security
	 * interceptors.
	 *
	 * Creates a filter chain with order = 0.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.cors()
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()

				// when using react build within maven
				.authorizeRequests().antMatchers("/", "/static/**", "/fonts/**").permitAll()

				// h2 console - develop only
				.antMatchers("/h2/**").permitAll()
				// swagger - develop only
				.antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
						"/swagger-ui", "/webjars/**", "/swagger-resources/configuration/ui", "/swagge‌​r-ui.html")
				.permitAll()

				// redundant: these are the same as in the ResourceServerConfig
				// required if testing frontend using react build within maven
				// required for permissions to work if using swagger
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.antMatchers("/api/me", "/api/me-account", "/api/me-account/update", "/api/specialization/**", "/api/diagnosis/**", "/api/ingredient/**").hasAuthority("ROLE_USER")
				.antMatchers("/api/patient/**").hasAnyAuthority("ROLE_PATIENT", "ROLE_SUPERADMIN")
				.antMatchers("/api/doctor/**").hasAnyAuthority("ROLE_DOCTOR", "ROLE_SUPERADMIN")
				.antMatchers("/api/pharmacist/**").hasAnyAuthority("ROLE_PHARMACIST", "ROLE_SUPERADMIN")
				.antMatchers("/api/admin/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_SUPERADMIN")

				// For paths NOT detailed above:
				// 1. we require viewers to login,
				// 2. any user with any role would be able to view
				// the requested url after a successful login.
				.anyRequest().authenticated().and()
				// Allow basic access authentication
				.httpBasic().and()
				// Disable cross-site request forgey protection
				.csrf().disable();
	}

}
