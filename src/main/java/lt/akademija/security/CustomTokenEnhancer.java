package lt.akademija.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.users.AbstractUser;

// just playing around - will remove later

public class CustomTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		
		Account account = (Account) authentication.getPrincipal();
		Map<String, Object> additionalInfo = new HashMap<>();
		
		Long userId = account.getId();
		
		additionalInfo.put("id", userId);
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
		
	}

}