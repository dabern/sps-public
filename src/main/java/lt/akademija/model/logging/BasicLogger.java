package lt.akademija.model.logging;
/*
package lt.akademija.model.Logging;

import lt.akademija.model.entity.users.Patient;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.awt.print.Pageable;
import java.util.Arrays;
import java.util.Iterator;

*/
/**
 * Aspect based class responsible for logging
 * of Controller, Service and Repository classes
 * Performs basic logging, containing: Class, method called, args passed, outcome of method and exception record
 *//*

@Component
@Aspect
public class BasicLogger {


    private static final Logger logger = Logger.getLogger("Logger");

    */
/**
     * Method for declaring annotated pointcuts
     *//*

    @Pointcut(value = "execution(* lt.akademija.service..*(..))" +
                    "||execution(* lt.akademija.repository..*(..))" +
                    "||execution(* lt.akademija.controller..*(..)))")
    private void logger(){};


    */
/**
     * All logging performed before method is called;
     * Used to check passed arguments
     * @param jp - parameter containing various info, including passed argument list
     *//*

    @Before("logger()")
    public void logBefore(JoinPoint jp){
        if (jp.getTarget().getClass().getSimpleName().contains("Proxy"))
            return;
        logger.info("Before method: " + jp.getSignature().getName()
                + ". Class: " + jp.getTarget().getClass().getSimpleName());
        //logger.info("Args:"+Arrays.toString(jp.getArgs()));
    }

    */
/**
     * Method invoked if exception is thrown
     * @param jp - parameter containing various info
     * @param ex - exception info
     *//*

    @AfterThrowing(pointcut = "logger()", throwing = "ex")
    public void logException(JoinPoint jp, Exception ex){
        logger.info("After throwing method: "
                + jp.getSignature().getName() + ". Class: "
                + jp.getTarget().getClass().getSimpleName());
        logger.info("Exception: " + ex.getMessage());
    }

    */
/**
     * Logging invoked after method quits
     * @param jp - parameter containing various info
     * @param result - captured outcome of the method
     *//*


    @AfterReturning(pointcut = "logger()",
            returning = "result")
    public void loggerFinallize(JoinPoint jp, Object result){
        if (jp.getTarget().getClass().getSimpleName().matches("Proxy"))
            return;
        logger.info("Class: " + jp.getTarget().getClass().getSimpleName());
        logger.info("Method called: " + jp.getSignature().getName());
        logger.info("Result type: "+ result.getClass().cast(result));
        //Breaks create doctor by iD
//        if(result.getClass().isArray())
//        {
//            Iterable list = Arrays.asList(result);
//            logger.info("Array contains:");
//            list.forEach((member)->logger.info("Member: "+member));
//        }
        if(result instanceof Page){
            Iterable list = ((Page) result).getContent();
            logger.info("Array contains:");
            list.forEach((member)->logger.info("Member: "+member));
        }else{
            logger.info("Result: "+ result);
        }
    }
}
*/
