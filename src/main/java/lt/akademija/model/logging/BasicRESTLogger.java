package lt.akademija.model.logging;
//package lt.akademija.model.Logging;
//
//import org.apache.log4j.Logger;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.*;
//import org.springframework.stereotype.Component;
//
//import java.util.Arrays;
//
//
//@Component
//@Aspect
//
//public class BasicRESTLogger {
//
//    private static final Logger logger = Logger.getLogger("Logger");
//
////    @Pointcut("execution(* lt.akademija.service..*(..))&& args(arg)")
////    public <T> void methodsWithArgs(T arg){}
//
////    @Pointcut("execution(* lt.akademija.service..*())")
////    public void methodsNoArgs(){}
//
////    //Track all methods in service package with single arg
////    @Around(value = "execution(* lt.akademija.controller..*(..))&& args(arg)")
////    public void logRESTargs(ProceedingJoinPoint jp){
////        try{
////            String method = jp.getSignature().toShortString();
////            //String className = jp.getSignature().getName();
////            // logger.info("Before start");
////            logger.info("Called in: "+method);
////            //logger.info("Arg passed: "+ Arrays.toString(jp.getArgs()));
////            jp.proceed();
////            // logger.info("After start");
////        }catch(Throwable e){
////            String method = jp.getSignature().toShortString();
////            logger.error("Error in: " + method);
////            logger.error("Details: "+e.getMessage());
////        }
////    }
//
//    //Track all parametless methods in service package
//    @Around(value = "execution(* lt.akademija.controller..*())")
//    public void logREST(ProceedingJoinPoint jp){
//        try{
//            String method = jp.getSignature().toShortString();
//            logger.info("Called in: "+method);
//            jp.proceed();
//        }catch(Throwable e){
//            String method = jp.getSignature().toShortString();
//            logger.error("Error in: " + method);
//            logger.error("Details: "+e.getMessage());
//        }
//    }
//}
