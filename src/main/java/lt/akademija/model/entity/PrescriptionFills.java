package lt.akademija.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.entity.users.Pharmacist;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode
@Entity
@Table(name = "PRESCRIPTION_FILLS")
public class PrescriptionFills implements Serializable {
    @EmbeddedId
    private PrescriptionFillsPK prescriptionFillsPK;

    //Needed
    public PrescriptionFills() {
    }

    //To synchronize bidirectional relationship fill must always be created like this to set primary key correctly
    // Needs to be TESTED
    @PersistenceConstructor
    public PrescriptionFills(Date date, Pharmacist pharmacist, Prescription prescription) {
        this.prescriptionFillsPK = new PrescriptionFillsPK(date, pharmacist, prescription);
//        pharmacist.addPrescriptionFill(this);
//        prescription.addPrescriptionFill(this);
    }

    public PrescriptionFillsPK getPrescriptionFillsPK() {
        return prescriptionFillsPK;
    }

    public void setPrescriptionFillsPK(PrescriptionFillsPK prescriptionFillsPK) {
        this.prescriptionFillsPK = prescriptionFillsPK;
    }
}
