package lt.akademija.model.entity.simple;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "SPECIALIZATIONS")
@EqualsAndHashCode
@Data
public class Specialization extends AbstractSimpleEntity {

    @Column(name = "DESCRIPTION", length  = 8000)
    private String description;

    @Override
    public String toString() {
        return "Specialization{" +
                "id='"+super.getId()+'\''+
                "title='" + super.getTitle() + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
