package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lt.akademija.model.entity.Account;
import java.util.Date;
import java.util.List;

@Getter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTOFull extends AccountDTO {

	private List<String> roles;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss", timezone = "Europe/Vilnius", locale = "lt")
	private Date createdOn;
	private boolean enabled;
	private String password; // never send to frontend - for password udptate as admin puposes only

	@JsonCreator
	public AccountDTOFull(
					@JsonProperty("username") String username, 
					@JsonProperty("roles") List<String> roles,
					@JsonProperty("createdOn") Date createdOn, 
					@JsonProperty("enabled") boolean enabled
				){
		super(username, null);
		System.out.println("Non default account constructor called");
		this.roles = roles;
		this.createdOn = createdOn;
		this.enabled = enabled;
	}

	public static AccountDTOFull toDTO(Account entity) {
		AccountDTOFull account = new AccountDTOFull(
					entity.getUsername(), 
					entity.getRoles(), 
					entity.getCreatedOn(),
					entity.getEnabled()
				);
		return account;
	}

}
