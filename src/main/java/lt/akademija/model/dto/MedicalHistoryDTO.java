package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.repository.DTOInterface.MedicalHistoryDTOInterface;

import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.TimeZone;

@Data
public class MedicalHistoryDTO {
    private Long patientId;
    private Long patientPid;
    private Long doctorId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    @Setter(AccessLevel.PRIVATE)
    private Date date;
    private Long diagnosisId;
    private String notes;
    private Integer appointmentLength;
    private Boolean compensated;
    private Boolean repeatVisitation;


    public MedicalHistoryDTO() {
    }

    @JsonCreator
    public MedicalHistoryDTO(@JsonProperty("patientId") Long patientId, @JsonProperty("patientPid") Long patientPid,
                             @JsonProperty("doctorId") Long doctorId,
                             @JsonProperty("diagnosisId") Long diagnosisId,
                             @JsonProperty("notes") String notes, @JsonProperty("appointmentLength") Integer appointmentLength,
                             @JsonProperty("compensated") Boolean compensated, @JsonProperty("repeatVisitation") Boolean repeatVisitation) {
        this.patientId = patientId;
        this.patientPid = patientPid;
        this.doctorId = doctorId;
        this.diagnosisId = diagnosisId;
        this.notes = notes;
        this.appointmentLength = appointmentLength;
        this.compensated = compensated;
        this.repeatVisitation = repeatVisitation;
    }

    @JsonIgnore
    public MedicalHistoryDTO(Long patientId, Long patientPid,
                             Long doctorId, Date date,
                             Long diagnosisId,
                             String notes, Integer appointmentLength,
                             Boolean compensated, Boolean repeatVisitation) {
        this.patientId = patientId;
        this.patientPid = patientPid;
        this.doctorId = doctorId;
        this.date = date;
        this.diagnosisId = diagnosisId;
        this.notes = notes;
        this.appointmentLength = appointmentLength;
        this.compensated = compensated;
        this.repeatVisitation = repeatVisitation;
    }

    public static MedicalHistoryDTO toDTO(MedicalHistory history) {
        MedicalHistoryDTO historyDTO = new MedicalHistoryDTO(history.getMedicalHistoryPK().getPatient().getId(),
                history.getMedicalHistoryPK().getPatient().getPid(),
                history.getMedicalHistoryPK().getDoctor().getId(),
                history.getMedicalHistoryPK().getTimestamp(),
                history.getMedicalHistoryPK().getDiagnosis().getId(),
                history.getNotes(),
                history.getAppointmentLength(),
                history.isCompensated(),
                history.isRepeatVisit());
        return historyDTO;
    }



    public static MedicalHistoryDTO toDTO(MedicalHistoryDTOInterface historyDTOInterface) {
        MedicalHistoryDTO historyDTO = new MedicalHistoryDTO();
        System.out.println("Medical history DTO constructor called");
        BeanUtils.copyProperties(historyDTOInterface, historyDTO);
        historyDTO.setDate(historyDTOInterface.getDate());
        System.out.println("After medical history DTO constructor");
        return historyDTO;
    }
}
