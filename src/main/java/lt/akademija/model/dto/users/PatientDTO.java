package lt.akademija.model.dto.users;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatientDTO extends AbstractUserDTO{

    public static PatientDTO toDTO(Patient patient)
    {
        System.out.println("DTO in PatientDTO constructor called");
        PatientDTO patientDTO = AbstractUserDTO.toDTO(patient,PatientDTO.class);
        patientDTO.setDob(patient.getDob());
        if(patient.getDoctor() != null)
        {
            System.out.println("Doctor not null");
            patientDTO.setDoctorId(patient.getDoctor().getId());
        }
        return patientDTO;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private  Date dob;
    private  Long doctorId;
    private Boolean hasAccount;
    private Long doctorPid;

    //This setter tricks BeanUtils still to copy doctor id if it exists;
    public void setDoctor(Doctor doctor)
    {
     if(doctor != null)
     {
         this.doctorId = doctor.getId();
         this.doctorPid = doctor.getPid();
     }
    }
}
