package lt.akademija.model.dto.users;


import lombok.*;
import lt.akademija.model.entity.users.Pharmacist;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(as = PharmacistDTO.class)
@Data
public class PharmacistDTO extends AbstractUserDTO{

    public static PharmacistDTO toDTO(Pharmacist pharmacist)
    {
        System.out.println("DTO constructor called");
        PharmacistDTO pharmacistDTO = AbstractUserDTO.toDTO(pharmacist, PharmacistDTO.class);
        pharmacistDTO.setOrganization(pharmacist.getOrganization());
        return pharmacistDTO;
    }

    private String organization;

}
