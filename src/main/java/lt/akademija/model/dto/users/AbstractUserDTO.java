package lt.akademija.model.dto.users;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "user")
@JsonSubTypes({@JsonSubTypes.Type(value = DoctorDTO.class, name = "doctor"),
        @JsonSubTypes.Type(value = PharmacistDTO.class, name = "pharmacist"),
        @JsonSubTypes.Type(value = PatientDTO.class, name = "patient"),
        @JsonSubTypes.Type(value = AdminDTO.class, name = "admin")})
@AllArgsConstructor
@NoArgsConstructor

public abstract class AbstractUserDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private Long pid;

    public static <T extends AbstractUserDTO, E> T toDTO(final E entity, Class<T> type) {
        try {
            T dto = type.newInstance();
            System.out.println("DTO constructor in AbstractUserDTO called");
            BeanUtils.copyProperties(entity, dto);
            return dto;
        } catch (Exception e) {
            System.out.println("Damn unsuccessful in AbstractUserDTO: " + e.getMessage());
        }
        return null;
    }
}
