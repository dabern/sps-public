package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lt.akademija.model.entity.Prescription;
import lt.akademija.repository.DTOInterface.PrescriptionDTOInterface;

import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
@EqualsAndHashCode
public class PrescriptionDTO{

	private Long prescriptionId;
    private Long activeIngredient;
    private Double activeIngredientPerDose;
    private String activeIngredientUnits;
    private String dosageNotes;
    private Long doctorId;
    private Long patientPid;
    private Long patientId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    @Setter(AccessLevel.PRIVATE)
    private Date prescriptionDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    private Date validUntil;
    private Long fillsNo;

    public static PrescriptionDTO toDTO(PrescriptionDTOInterface prescriptionDTOInterface)
    {
        PrescriptionDTO prescription = new PrescriptionDTO();
        System.out.println("DTO constructor called");
        BeanUtils.copyProperties(prescriptionDTOInterface, prescription);
        prescription.setPrescriptionId(prescriptionDTOInterface.getId());
        prescription.setPrescriptionDate(prescriptionDTOInterface.getPrescriptionDate());
        return prescription;
    }
}
