package lt.akademija.model.dto.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@JsonDeserialize(as = PatientSearchDTO.class)
public class PatientSearchDTO  extends BasicUserSearchDTO{
    private String diagnosisTitle;
    private Long doctorId;
}
