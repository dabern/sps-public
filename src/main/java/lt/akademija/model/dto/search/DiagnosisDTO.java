package lt.akademija.model.dto.search;

import lombok.Data;

@Data
public class DiagnosisDTO {

    private String title;

    private String description;

}
