package lt.akademija.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lt.akademija.model.entity.PrescriptionFills;
import lt.akademija.repository.DTOInterface.PrescriptionFillsDTOInterface;

import org.springframework.beans.BeanUtils;

import java.util.Date;

@Data
public class PrescriptionFillsDTO {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Europe/Vilnius", locale = "lt")
    @Setter(AccessLevel.PRIVATE)
    private Date date;
    private Long prescriptionId;
    private Long phramacistId;

    public static PrescriptionFillsDTO toDTO(PrescriptionFillsDTOInterface prescriptionFillsDTOInterface)
    {
        PrescriptionFillsDTO prescriptionFills = new PrescriptionFillsDTO();
        System.out.println("DTO constructor called");
        BeanUtils.copyProperties(prescriptionFillsDTOInterface, prescriptionFills);
        prescriptionFills.setDate(prescriptionFillsDTOInterface.getDate());
        return prescriptionFills;
    }

    public static PrescriptionFillsDTO toDTO(PrescriptionFills fills)
    {
        PrescriptionFillsDTO fillsDTO = new PrescriptionFillsDTO();
        System.out.println("DTO constructor called");
        fillsDTO.setPhramacistId(fills.getPrescriptionFillsPK().getPharmacist().getId());
        fillsDTO.setPrescriptionId(fills.getPrescriptionFillsPK().getPrescription().getId());
        fillsDTO.setDate(fills.getPrescriptionFillsPK().getTimestamp());
        return fillsDTO;
    }
}
