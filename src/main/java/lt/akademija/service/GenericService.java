package lt.akademija.service;
//package lt.akademija.service.serviceEntity;
//
//import lt.akademija.repository.GenericRepository;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//
//
///**
// * Generic class used for Complex entities Medical History, Prescriptions, Prescription Fills, which can be sorted just by pid.
// * @param <T> - entity, extending abstract user
// */
//
//abstract public class GService<T>/* implements Find<T> */{
//    final private Class<T> type;
//
//    GService(Class<T> type) {
//        this.type = type;
//    }
//
//    @Autowired
//    protected GenericRepository<T> genericRepository;
//
//    /**
//     * Finds all entities
//     *
//     * @param pageRequest - carries paging information;
//     * @return - paged results;
//     */
//    public Page<T> findAll(Pageable pageRequest) {
//        return genericRepository.findAll(pageRequest);
//    }
//
//    /**
//     * Method implementation of Find interface, used to convert DTO to entity
//     * @param id - objects id in the table
//     * @return entity
//     */
//
//   // @Override
//    public T findById(Long id){
//        return genericRepository.findOne(id);
//    }
//
//    /**
//     * Method implementation of Find interface, used to convert DTO to entity, for entities having pid.
//     * Otherwise method returns null value;
//     * @param pid - entity pid
//     * @return entity
//     */
//   /* @Override
//    public T findByPid(Long pid) {
//        return null;
//    }*/
//
//
//
//    /**
//     * Method used to create entity in the db
//     * @param entity - passed entity
//     * @return if successful returns, created entity
//     */
//    public T create(T entity) {
//        System.out.println(entity.toString());
//        System.out.println("Create in generic service called");
//        genericRepository.save(entity);
//        return entity;
//    }
//
//    /**
//     * Method used to update entity, by its id
//     * @param id - entity id
//     * @param entity - updated version of entity
//     * @return if successful returns updated entity
//     */
//
//    public T update(Long id, T entity){
//        T oldObject = genericRepository.findOne(id);
//        BeanUtils.copyProperties(entity, oldObject, "id");
//        genericRepository.save(oldObject);
//        return oldObject;
//    }
//
//    /**
//     * Method implementation of Find interface. Method is used to determine if class can operate with desired objects
//     * @param type - Object class
//     * @return returns true if service can handle desired class
//     */
///*
//    @Override
//    public boolean canHandle(Type type) {
//        return this.type.equals(type);
//    }
//
//    *//**
//     * Class has info on entity it works on. This method returns entity class, this service works on.
//     * @return - class of the object service works on
//     *//*
//    public Class<T> getType() {
//        return type;
//    }*/
//
//}
