package lt.akademija.service.Facade;

import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.dto.search.BasicUserSearchDTO;
import lt.akademija.model.dto.users.AbstractUserDTO;
import lt.akademija.model.dto.users.DoctorDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.simple.AbstractSimpleEntity;
import lt.akademija.model.entity.users.AbstractUser;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.service.AccountService;
import lt.akademija.service.simple.GenericSimpleService;
import lt.akademija.service.users.DoctorService;
import lt.akademija.service.users.GenericUserService;
import lt.akademija.service.users.PatientService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.security.auth.login.AccountException;

import java.util.List;

// exceptions done
/**
 * A facade providing a unified and simplified interface for all complex
 * functionality needed by the admin user.
 * <p>
 * <p>
 * Admin facade controls all classes extending GenericUserDTOService, which are
 * responsible, for operation with users. Classes representing users usually
 * contain user name in it. The choice of the correct user service is based on
 * that.
 */
@Service
public class AdminFacade {

	private static final Logger logger = LogManager.getLogger(AdminFacade.class);

	@Autowired
	private AccountService accountService;

	// Collect all DTO projection services which are initialized by classes
	// extending AbstractUserDTO;
	@Autowired
	private List<GenericUserService<?, ? extends AbstractUserDTO>> userServiceList;

	// Collect all classes extending GSimpleService
	@Autowired
	private List<? extends GenericSimpleService<? extends AbstractSimpleEntity>> simpleServiceList;

	/**
	 * Method lists all autowired classes
	 */
	@SuppressWarnings("rawtypes")
	@PostConstruct
	void listAll() {
		logger.info("listAll() in AdminFacade called. ");
		logger.info("These are the autowired GenericUserServices");
		for (GenericUserService gen : userServiceList)
			logger.info(gen.getType().getSimpleName());
	}

	/**
	 * Checks weather user type passed by url is contained in the class name used to
	 * represent it in the code.
	 *
	 * @param className
	 *            - class name short form string;
	 * @param url
	 *            - class name posted in the url;
	 * @return object DTO projection;
	 */
	private boolean canHandle(String className, String url) {
		return className.toLowerCase().contains(url);
	}

	/**
	 * Finds which method can work with given user type.
	 *
	 * @param url
	 *            - class name posted in the url;
	 * @param <T>
	 *            - DTO class extending AbstractUserDTO;
	 * @return object DTO projection;
	 */
	@SuppressWarnings("unchecked")
	private <T extends AbstractUserDTO> GenericUserService<?, T> chooseUserService(String url) throws Exception {
		String className;
		for (GenericUserService<?, ? extends AbstractUserDTO> user : userServiceList) {
			className = user.getType().getSimpleName();
			if (canHandle(className, url)) {
				return (GenericUserService<?, T>) user;
			}
		}
		throw new Exception(
				"Could not determine which service to use. Make sure \"user\" parameter is given without typos.");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <G extends GenericSimpleService<? extends AbstractSimpleEntity>> G chooseSimpleService(String entityType)
			throws Exception {
		entityType = entityType.replaceAll("\\s+", "").toLowerCase();
		String serviceType = null;
		for (GenericSimpleService/* <? extends AbstractSimpleEntity> */ service : simpleServiceList) {
			serviceType = service.getClass().getSimpleName().toLowerCase();
			if (serviceType.contains(entityType)) {
				return (G) service;
			}
		}
		throw new Exception(
				"Could not determine simple service to use. Make sure \"entityType\" parameter is given without typos");
	}

	// #####################################################
	// ############## GET METHODS #############################
	// #####################################################

	/**
	 * Returns a user by id given
	 *
	 * @param className
	 *            - class name short form string;
	 * @param userId
	 *            - user id in the db;
	 * @param <T>
	 *            - DTO object class;
	 * @return DTO projection;
	 * @throws Exception
	 *             if user type with id provided does not exist
	 */
	public <T extends AbstractUserDTO> T getUserById(String className, Long userId) throws Exception {
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		return service.findByIdDTO(userId);
	}

	/**
	 * Returns a user by pid provided
	 *
	 * @param className
	 *            - class name short form string;
	 * @param pid
	 *            - personal id of user;
	 * @param <T>
	 *            - DTO object class;
	 * @return DTO projection;
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> T getUserByPid(String className, Long pid) throws Exception {
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		return service.findByPidDTO(pid);
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws AccountException
	 *             if account with id provided does not exist
	 */
	public AccountDTOFull getAccountById(Long userId) throws AccountException {
		try {
			return accountService.getAccountDTOById(userId);
		} catch (AccountException e) {
			throw e;
		}
	}

	// #########################################################
	// ################# SEARCH METHODS ########################
	// #########################################################

	/**
	 * Finds user by combination of regex applied to its firstname, lastname and pid
	 * fields. The fields may only start with text or numbers typed
	 *
	 * @param className
	 *            - name of the user grupe, where the search should be executed; //
	 *            * @param params - parameters passed in the Array of Strings.
	 *            !.FirstName; 2.LastName; 3.Pid;
	 * @param pageRequest
	 *            - paging info;
	 * @return - paged result;
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	public <T extends AbstractUserDTO> Page<T> searchUsersByNameOrSurnameOrPid(String className,
			BasicUserSearchDTO searchDTO, Pageable pageRequest) throws Exception {
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		if (service.canHandle(Patient.class)) {
			Page<PatientDTO> patientDTO = (Page<PatientDTO>) service.searchUsersByNameOrSurnameOrPid(searchDTO,
					pageRequest);
			patientDTO.forEach(p -> p.setHasAccount(accountService.existsById(p.getId())));
			return (Page<T>) patientDTO;
		} else
			return service.searchUsersByNameOrSurnameOrPid(searchDTO, pageRequest);
	}

	// ########################################################
	// ############ CREATE METHODS ############################
	// ########################################################

	/**
	 * Create a new user without an associated account;
	 *
	 * @param className
	 *            - class name short form string; //* @param user - DTO projection
	 * @param <T>
	 *            - user entity projection class, extending AbstractUserDTO;
	 * @return - returns relevant user id
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> Long createPatient(String className, PatientDTO patientDTO) throws Exception {
		className = className.trim().toLowerCase();

		GenericUserService<?, PatientDTO> patientService = chooseUserService("patient");
		GenericUserService<?, DoctorDTO> doctorService = chooseUserService("doctor");

		Long givenPid = patientDTO.getPid();
		Patient patientWithGivenPid = ((PatientService) patientService).findByPid(givenPid);
		if (!(patientWithGivenPid == null)) {
			throw new Exception("Patient with pid " + givenPid + " already exists.");
		}

		Long doctorId = patientDTO.getDoctorId();
		Long doctorPid = patientDTO.getDoctorPid();

		if ((doctorId == null) || (doctorId <= 0)) {
			if ((doctorPid == null) || (doctorPid <= 0)) {
				doctorId = null;
				System.out.println("#################################");
				System.out.println("INFO: doctor was not provided. Creating patient without a doctor.");
				System.out.println("#################################");
			} else {
				Doctor doctorFound = ((DoctorService) doctorService).findByPid(doctorPid);
				System.out.println("#################################");
				System.out.println("FOUND DOCTOR: " + doctorFound);
				System.out.println("#################################");
				if (doctorFound == null) {
					throw new Exception("No doctor with pid: " + doctorPid + " exists!");
				} else {
					patientDTO.setDoctorId(doctorFound.getId());
				}
			}
		} else {
			Doctor doctorFound = ((DoctorService) doctorService).findById(doctorId);
			if (doctorFound == null) {
				throw new Exception("No doctor with id: " + doctorId + " exists!");
			}
		}

		System.out.println("#################################");
		System.out.println("ABOUT TO CREATE PATIENT");
		System.out.println("PATIENT DTO AT THIS POINT");
		System.out.println("First name" + patientDTO.getFirstName());
		System.out.println("Last name" + patientDTO.getLastName());
		System.out.println("Doctor id" + patientDTO.getDoctorId());
		System.out.println("Doctor pid" + patientDTO.getDoctorPid());
		System.out.println("Id" + patientDTO.getId());
		System.out.println("Pid" + patientDTO.getPid());
		System.out.println("Dob" + patientDTO.getDob());
		System.out.println("#################################");
		System.out.println("#################################");
		PatientDTO patientCreated = patientService.createUser(patientDTO);

		return patientCreated.getId();
	}

	/**
	 * Create new user with an associated account (except for patient); Patient
	 * account must be creted on a seperate method - createPatientAccount Username
	 * and password sent along with PatientDTO will be ignored
	 *
	 * @param className
	 *            - class name short form string; //* @param user - DTO projection
	 * @param <T>
	 *            - user entity projection class, extending AbstractUserDTO;
	 * @param username
	 *            - desired username
	 * @param password
	 *            - desired password
	 * @return - returns relevant user id
	 */
	public <T extends AbstractUserDTO> Long createUser(String className, T userDTO, String username, String password)
			throws Exception {
		logger.info("Creating user of type " + className + " from dto " + " userDTO");
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);

		Long givenPid = userDTO.getPid();
		AbstractUser entityWithGivenPid = service.findByPid(givenPid);
		if (!(entityWithGivenPid == null)) {
			throw new Exception(className + " with pid " + givenPid + " already exists!");
		} else {
			Account userAccount = new Account();
			userAccount.setUsername(username);
			userAccount.setPassword(password);
			userAccount.grantAuthority("ROLE_USER");
			switch (className) {
			case "doctor":
				userAccount.grantAuthority("ROLE_DOCTOR");
				break;
			case "admin":
				userAccount.grantAuthority("ROLE_ADMIN");
				break;
			case "pharmacist":
				userAccount.grantAuthority("ROLE_PHARMACIST");
				break;
			// if className was patient, other method supposed to be called
			case "patient":
				throw new Exception("patient ended up in the wrong method");
			}
			AbstractUser userCreated = service.createEntity(userDTO);
			Long userCreatedId = userCreated.getId();
			logger.debug("Id of user created: " + userCreatedId);
			try {
				accountService.createAccount(userAccount, userCreated);
				return userCreatedId;
			} catch (AccountException e) {
				service.removeById(userCreatedId);
				throw e;
			}
		}
	}

	/**
	 * Creates and associates an account with the user provided
	 *
	 * @param patientId
	 *            - patient Id associated with the account to-be
	 * @param accountDTO
	 *            - contains username and password for the account to-be
	 * @return - returns relevant user id
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> Long createPatientAccount(Long patientId, AccountDTO accountDTO)
			throws Exception {

		if (accountService.existsById(patientId)) {
			throw new AccountException(String.format("Account with id [%s] already exists", patientId));
		}

		GenericUserService<?, T> service = chooseUserService("patient");
		Patient patientFound = (Patient) service.findEntityById(patientId);
		Account patientAccount = new Account();
		patientAccount.setUsername(accountDTO.getUsername());
		patientAccount.setPassword(accountDTO.getPassword());
		patientAccount.grantAuthority("ROLE_USER");
		patientAccount.grantAuthority("ROLE_PATIENT");
		Account accountCreated = accountService.createAccount(patientAccount, patientFound);
		return accountCreated.getId();
	}

	public <T extends AbstractSimpleEntity> Long createSimpleEntity(String entityType, T entity) throws Exception {
		entityType = entityType.trim().toLowerCase();
		GenericSimpleService<T> serviceChosen = chooseSimpleService(entityType);
		String serviceCalled = serviceChosen.getClass().getSimpleName();
		String titleProvided = entity.getTitle();
		T entityWithSameTitle = serviceChosen.findByTitle(titleProvided);
		if (!(entityWithSameTitle == null)) {
			throw new Exception("Entity with title " + titleProvided + " already exists.");
		}
		System.out.println("############");
		System.out.println("Service chosen: " + serviceCalled);
		System.out.println("############");
		T entityCasted = serviceChosen.castThis(entity);
		T entityCreated = serviceChosen.create(entityCasted);
		return entityCreated.getId();
	}

	// #####################################################
	// ############ UPDATE METHODS #############################
	// #####################################################

	/**
	 * Updates username and password for the account specified.
	 *
	 * @param userId
	 *            - user id used to specify the account in question
	 * @param accountDTO
	 *            - DTO object containing
	 * @return - returns user id provided
	 */
	public Account updateUserAccountById(Long userId, AccountDTO accountDTO) throws AccountException {
		return accountService.updateById(userId, accountDTO);
	}

	/**
	 * Finds user by its id and updates it
	 *
	 * @param className
	 *            - short string name of the class;
	 * @param id
	 *            - user id provided;
	 * @param user
	 *            - user DTO;
	 * @param <T>
	 *            - DTO class;
	 * @return user DTO;
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> T updateUserById(String className, Long id, T user) throws Exception {
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		// GUserService<?,PatientDTO> service = chooseService("patient");
		T patient = service.update(id, user);
		System.out.println("In faced: " + patient.getClass());
		// System.out.println("Patient DATE in Facade: " + patient.getDob());
		return patient;
	}

	/**
	 * Finds user by its pid and updates it
	 *
	 * @param className
	 *            - short string name of the class;
	 * @param pid
	 *            - user pid provided
	 * @param user
	 *            - userDTO
	 * @return userDTO;
	 * @throws Exception
	 */
	public <T extends AbstractUserDTO> T updateUserByPid(String className, Long pid, T user) throws Exception {
		className = className.trim().toLowerCase();
		GenericUserService<?, T> service = chooseUserService(className);
		return service.updateByPid(pid, user);
	}

	public <T extends AbstractSimpleEntity> T updateSimpleEntityById(String entityType, Long id, T entity)
			throws Exception {
		entityType = entityType.trim().toLowerCase();
		@SuppressWarnings("unchecked")
		GenericSimpleService<T> serviceChosen = (GenericSimpleService<T>) chooseSimpleService(entityType);

		String titleProvided = entity.getTitle();
		T entityWithSameTitle = serviceChosen.findByTitle(titleProvided);
		if (!(entityWithSameTitle == null)) {
			throw new Exception("Another entity with title " + titleProvided + " already exists.");
		}

		T entityCasted = serviceChosen.castThis(entity);
		T entityUpdated = serviceChosen.update(id, entityCasted);
		return entityUpdated;
	}
	
	public void setEnabledById(Long accountId, boolean newEnabled) throws AccountException {
		Account account = accountService.getAccountById(accountId);
		if (account == null) {
			throw new AccountException("No account with id " + accountId + " exists.");
		} else {
			boolean currentEnabled = account.getEnabled();
			if (currentEnabled == newEnabled) {
				return;
			} else {
				accountService.switchEnabledById(accountId);
			}
		}
	}
	
	public void resetPasswordById(Long accountId) throws AccountException {
		Account account = accountService.getAccountById(accountId);
		if (account == null) {
			throw new AccountException("No account with id " + accountId + " exists.");
		} else {
			accountService.resetPasswordById(accountId);
		}
	}

}
