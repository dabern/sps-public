package lt.akademija.service;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.repository.other.PrescriptionRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

// done logging
// done exceptions
@Service
public class PrescriptionService {

	private static final Logger logger = LogManager.getLogger(PrescriptionService.class);

	@Autowired
	private PrescriptionRepository repository;

	@Autowired
	private PrescriptionFillsService fillsService;

	public Page<PrescriptionDTO> findAllByPatientId(Long id, Pageable pageRequest) {
		logger.info("PrescriptionService attempting to find all prescriptions with patient id: " + id);
		return repository.findAllByPatient_Id(id, pageRequest).map(Entity -> {
			PrescriptionDTO pr = PrescriptionDTO.toDTO(Entity);
			pr.setFillsNo(fillsService.findFillsNoByPrescriptionId(pr.getPrescriptionId()));
			return pr;
		});
	}

	public Page<PrescriptionDTO> findAllByPatientPid(Long pid, Pageable pageRequest) {
		logger.info("PrescriptionService attempting to find all prescriptions with patient pid: " + pid);
		return repository.findAllByPatient_Pid(pid, pageRequest).map(Entity -> {
			PrescriptionDTO pr = PrescriptionDTO.toDTO(Entity);
			pr.setFillsNo(fillsService.findFillsNoByPrescriptionId(pr.getPrescriptionId()));
			return pr;
		});
	}

	public PrescriptionDTO createFromDTO(PrescriptionDTO prescriptionDTO) {
		logger.info("PrescriptionService attempting to create a new prescription from dto: " + prescriptionDTO);
		return repository.saveDTO(prescriptionDTO);
	}

	public PrescriptionDTO findByPrescriptionId(Long prescriptionId) {
		logger.info("PrescriptionService attempting to find a prescription with id: " + prescriptionId);
		PrescriptionDTO pr = PrescriptionDTO.toDTO(repository.findOneById(prescriptionId));
		if (pr != null) {
			logger.debug("Prescription with id " + prescriptionId + " found.");
			pr.setFillsNo(fillsService.findFillsNoByPrescriptionId(pr.getPrescriptionId()));
		} else {
			logger.debug("Prescription with id " + prescriptionId + " was not found.");
		}
		return pr;
	}

}
