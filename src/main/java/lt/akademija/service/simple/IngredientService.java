package lt.akademija.service.simple;

import org.springframework.stereotype.Service;

import lt.akademija.model.entity.simple.Ingredient;

@Service
public class IngredientService extends GenericSimpleService<Ingredient/*, IngredientRepository*/> {
    public IngredientService() {
        super(Ingredient.class);
    }
}
