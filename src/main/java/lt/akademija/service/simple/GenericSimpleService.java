package lt.akademija.service.simple;

import lt.akademija.model.entity.simple.AbstractSimpleEntity;
import lt.akademija.repository.GenericRepository;
import lt.akademija.repository.simple.GenericSimpleEntityRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Type;

/**
 * Abstract class declaring general CRUD service methods;
 * This generic service class is for simple classes:
 * Diagnosis, Specialization, Ingredient extend this class to realize CRUD(no projection to DTO used for these entities)
 * @param <T> - Entity.
 */

public abstract class GenericSimpleService<T>{

    //Access to entity repository
    @Autowired
    protected GenericSimpleEntityRepository<T> genericRepository;

    private Class<T> type;

    public GenericSimpleService(Class<T> type) {
        this.type = type;
    }

    /**
     * Finds all entities
     *
     * @param pageRequest - carries paging information;
     * @return - paged results;
     */
    public Page<T> findAll(Pageable pageRequest) {
        return genericRepository.findAll(pageRequest);
    }

    /**
     * Method implementation of Find interface, used to convert DTO to entity
     * @param id - objects id in the table
     * @return entity
     */

    public T findById(Long id){
        return genericRepository.findOne(id);
    }
    
    /**
     * Doco ner. Nieka ner
     * @param title
     * @return
     */
    public T findByTitle(String title) {
    	return genericRepository.findByTitle(title);
    }
    
    public Page<T> searchByTitle(String title, Pageable pageRequest) {
    	return genericRepository.findAllByTitleStartsWithIgnoreCase(title, pageRequest);
    }

    /**
     * Method implementation of Find interface, used to convert DTO to entity, for entities having pid.
     * Otherwise method returns null value;
     * @param pid - entity pid
     * @return entity
     */
    public T findByPid(Long pid) {
        return null;
    }

    /**
     * Method used to create entity in the db
     * @param entity - passed entity
     * @return if successful returns, created entity
     */
    public T create(T entity) {
        System.out.println(entity.toString());
        System.out.println("Create in generic service called");
        genericRepository.save(entity);
        return entity;
    }

    /**
     * Method used to update entity, by its id
     * @param id - entity id
     * @param entity - updated version of entity
     * @return if successful returns updated entity
     */

    public T update(Long id, T entity){
        T oldObject = genericRepository.findOne(id);
        BeanUtils.copyProperties(entity, oldObject, "id");
        genericRepository.save(oldObject);
        return oldObject;
    }

//    /**
//     * Method implementation of Find interface. Method is used to determine if class can operate with desired objects
//     * @param type - Object class
//     * @return returns true if service can handle desired class
//     */
//
//    @Override
//    public boolean canHandle(Type type) {
//        return this.type.equals(type);
//    }

    /**
     * Class has info on entity it works on. This method returns entity class, this service works on.
     * @return - class of the object service works on
     */
    public Class<T> getType() {
        return type;
    }
    
    @SuppressWarnings("unchecked") // i know what i'm doing
	public <S extends AbstractSimpleEntity> T castThis(S entityGiven) {
    	return (T) entityGiven;
    }

}
