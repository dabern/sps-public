package lt.akademija.service.simple;

import org.springframework.stereotype.Service;

import lt.akademija.model.entity.simple.Specialization;

@Service
public class SpecializationService extends GenericSimpleService<Specialization/*, SpecializationRepository*/> {
    public SpecializationService() {
        super(Specialization.class);
    }

}
