package lt.akademija.service.simple;

import org.springframework.stereotype.Service;

import lt.akademija.model.entity.simple.Diagnosis;

@Service
public class DiagnosisService extends GenericSimpleService<Diagnosis/*, DiagnosisRepository*/> {
    public DiagnosisService() {
        super(Diagnosis.class);
    }
}


