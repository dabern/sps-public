package lt.akademija.service.users;

import lt.akademija.model.dto.search.PatientSearchDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.PatientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class PatientService extends GenericUserService<Patient, PatientDTO> {

    public PatientService() {
        super(Patient.class, PatientDTO.class);
    }
    
    @Autowired
    private PatientRepository patientRepository;


    public Page<MedicalHistory> findMedicalRecords(Pageable pageRequest, Long pid)
    {
        return ((PatientRepository)genericRepository).findMedicalRecordsByPid( pageRequest, pid);
    }

    public PatientDTO updateByIdDTO(Long id, PatientDTO entity){
        PatientDTO user =patientRepository.updateDTOById(id,entity);
        System.out.println("PatientDTO date in patientsService: "+ user.getDob());
        return user;
    }
    public Page<Prescription> findPrescriptions(Pageable pageRequest, Long pid){
        return ((PatientRepository)genericRepository).findPrescriptionListByPid(pageRequest, pid);
    }

    //Keistas
    public Page<PatientDTO> findPatientList(Long pid, Pageable pageRequest){
        return ((PatientRepository)genericRepository).findPatientListByPid(pid, pageRequest).map(PatientDTO::toDTO);
    }

    public PatientDTO createDTO(PatientDTO patientDTO) {
        return ((PatientRepository) genericRepository).saveDTO(patientDTO);
    }
//    public Page<PatientDTO> findPatientListByPid(Long pid, Pageable pageRequest){
//        return patientRepository.findPatientListByPid(pid, pageRequest).map(PatientDTO::toDTO);
//    }

//    public Page<PatientDTO> findPatientListById(Long id, Pageable pageRequest){
//        return patientRepository.findPatientListById(id, pageRequest).map(PatientDTO::toDTO);
//    }

    public Page<PatientDTO> searchByNameAndSurnameAndPidAndDiagnosis(PatientSearchDTO patient, Pageable pageRequest) {
    	
    	String firstName = "";
    	String lastName = "";
    	String pidString = "";
     
        if ( (patient.getPid() == null) || (patient.getPid() < 1) ) {
        	pidString = "";
        } else {
        	pidString = String.valueOf(patient.getPid());
        }
        if ( !(patient.getFirstName()==null) ) {
			firstName = patient.getFirstName().trim();
		}
		if ( !(patient.getLastName()==null) ) {
			lastName = patient.getLastName().trim();
		}
		
		String diagnosisTitle = patient.getDiagnosisTitle();
		Long doctorId = patient.getDoctorId();
        
        if( (patient.getDiagnosisTitle() == null) || (patient.getDiagnosisTitle().isEmpty()) ) {
        	System.out.println("###########################################");
         	System.out.println("searchByNameAndSurnameAndPid invoked");
         	System.out.println("###########################################");
        	return patientRepository.searchByNameAndSurnameAndPid(firstName,lastName, pidString,
                    patient.getDoctorId(), pageRequest).map(PatientDTO::toDTO);
        } else {
        	System.out.println("###########################################");
         	System.out.println("searchByNameAndSurnameAndPidAndDiagnosis invoked");
         	System.out.println("###########################################");
        	return patientRepository.searchByNameAndSurnameAndPidAndDiagnosis(firstName, lastName,pidString,
                    diagnosisTitle, doctorId, pageRequest).map(PatientDTO::toDTO);
        }
    }

//    public Page<PatientDTO> searchByNameAndSurnameAndPid(String firstName, String lastName, String pid,  Long doctorId, Pageable pageRequest) {
//        System.out.println(
//                "Query parameters passed in patient Patient service: "+
//                        "firstname: "+firstName+
//                        "lastname: "+lastName+
//                        "pid: "+ pid+
//                        "doctorId: "+doctorId
//        );
//        return patientRepository.searchByNameAndSurnameAndPid(firstName, lastName, pid, doctorId, pageRequest).map(PatientDTO::toDTO);
//    }
    

    public Page<PatientDTO> findAllByDob(Pageable pageRequest) {
        return patientRepository.findAllByOrderByDobAsc(pageRequest).map(PatientDTO::toDTO);
    }

    //{
//        Doctor doctor = (Doctor)doctorService.findById(doctorId);
//        patient.setDoctor(doctor);
//        genericRepository.save(patient);
//    }
//
    public Page<PatientDTO> findPatientsByDoctorId(Long doctorId, Pageable pageRequest) {
        return patientRepository.findPatientListByDoctorId(doctorId, pageRequest).map(PatientDTO::toDTO);
    }

//    public Patient findDoctorsPatientByPid(Long doctorPid, Long patientPid) {
//        return genericRepository.findByDoctor_idAndPid(doctorPid, patientPid);
//    }
//
//    public List<Patient> findByDisease(String code) {
//        return genericRepository.findAllByDisease(code);
//    }
//
//    public Page<Patient> findByFirstName(Long doctorId, String firstName, Pageable pageRequest) {
//        return genericRepository.findAllByDoctor_idAndFirstNameStartingWithIgnoreCase(doctorId, firstName, pageRequest);
//    }
//
//    public List<Patient> findByLastName(Long doctorId, String lastName) {
//        return genericRepository.findAllByDoctor_idAndLastNameStartingWithIgnoreCase(doctorId, lastName);
//    }
}
