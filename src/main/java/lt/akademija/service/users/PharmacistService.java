package lt.akademija.service.users;

import lt.akademija.model.dto.users.PharmacistDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.users.Pharmacist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PharmacistService extends GenericUserService<Pharmacist, PharmacistDTO> {

    @Autowired
    private PatientService patientService;

    public PharmacistService() {
        super(Pharmacist.class, PharmacistDTO.class);
    }

    public Page<Prescription> findAllPrescriptions(Pageable pageRequest, Long pid){
        return patientService.findPrescriptions(pageRequest, pid);
    }

}
