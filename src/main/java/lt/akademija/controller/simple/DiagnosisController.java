package lt.akademija.controller.simple;

import io.swagger.annotations.Api;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.service.simple.DiagnosisService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "diagnoses")
@RequestMapping(value = "api/diagnoses/")
public class DiagnosisController extends GenericSimpleEntityController<Diagnosis> {
}
