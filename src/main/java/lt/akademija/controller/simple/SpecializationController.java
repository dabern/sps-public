package lt.akademija.controller.simple;

import io.swagger.annotations.Api;
import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.service.simple.SpecializationService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "specializations")
@RequestMapping(value = "api/specializations/")
public class SpecializationController extends GenericSimpleEntityController<Specialization> {
}
