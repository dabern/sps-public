package lt.akademija.controller.users;

import io.swagger.annotations.*;
import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.dto.MedicalHistoryPkDTO;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.dto.search.PatientSearchDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.service.AccountService;
import lt.akademija.service.MedicalHistoryService;
import lt.akademija.service.PrescriptionFillsService;
import lt.akademija.service.PrescriptionService;
import lt.akademija.service.users.PatientService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

// logger done
// exceptions done
@RestController
@Api(value = "doctor")
@RequestMapping(value = "api/doctor/")
public class DoctorController {

	private static final Logger logger = LogManager.getLogger(DoctorController.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private PrescriptionService prescriptionService;

	@Autowired
	private PrescriptionFillsService prescriptionFillsService;

	@Autowired
	private MedicalHistoryService medicalHistoryService;

	@ApiOperation(value = "Lists all patients of an authenticated doctor.", notes = "Returns list of patients")
	@GetMapping(value = "/patient/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PatientDTO> findPatient(Pageable pageable) {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Doctor with id " + doctorId + " is attempting to retrieve one's patient list.");
		return patientService.findPatientsByDoctorId(doctorId, pageable);
	}

	@ApiOperation(value = "View medical history of a particular patient of an authenticated doctor.")
	@GetMapping(value = "/patient/{patientPid}/medical-record/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<MedicalHistoryDTO> findPatientMedicalHistoryByPatientPid(@PathVariable Long patientPid,
			Pageable pageRequest) throws Exception {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Doctor with id " + doctorId + " is attempting to retrieve medical history of patient with pid "
				+ patientPid);
		PatientDTO patient = patientService.findByPidDTO(patientPid);
		if (patient == null) {
			throw new NullPointerException("No such patient exists");
		} else {
			logger.debug("Patient with pid " + patientPid + " was found.");
		}
		if (patient.getDoctorId().equals(doctorId)) {
			logger.debug("Doctor with id " + doctorId + " is authorized to access details of patient with pid " + patientPid);
			return medicalHistoryService.findByPatientPid(patientPid, pageRequest);
		} else {
			throw new Exception("Unauthorized access:  doctor is not authorized to view details of patient: "
					+ patient.getFirstName() + " " + patient.getLastName() + " with pid " + patient.getPid()
					+ " and id " + patient.getId());
		}
	}

	@ApiOperation(value = " View particular medical record details of one of my patients", notes = "Any doctor/patient ids sent will be ignored")
	@PostMapping(value = "/patient/{patientPid}/medical-record")
	public MedicalHistoryDTO findPatientMedicalRecordByPk(@PathVariable("patientPid") Long patientPid,
			@RequestBody MedicalHistoryPkDTO dto) throws Exception {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Doctor with id " + doctorId + " is attempting to medical record with PK " + dto
				+ " of patient with pid " + patientPid);
		PatientDTO patient = patientService.findByPidDTO(patientPid);
		if (patient == null) {
			throw new NullPointerException("No such patient exists");
		} else {
			logger.debug("Patient with pid " + patientPid + " was found.");
		}
		Long patientId = patient.getId();
		Long diagnosisId = dto.getDiagnosisId();
		Date date = dto.getDate();
		if (patient.getDoctorId().equals(doctorId)) {
			logger.debug("Doctor with id " + doctorId + " is authorized to access details of patient with pid " + patientPid);
			MedicalHistoryDTO recordFound = medicalHistoryService.findByPk(patientId, doctorId, diagnosisId, date);
			if (recordFound == null) {
				throw new NullPointerException("No medical record with PK " + dto + " was found.");
			} else {
				logger.debug("Medical record with PK " + dto + " was founnd.");
				return recordFound;
			}
		} else {
			throw new Exception("Unauthorized access:  doctor is not authorized to view details of patient: "
					+ patient.getFirstName() + " " + patient.getLastName() + "with pid " + patient.getPid());
		}
	}

	@ApiOperation(value = "View prescription history of a particular patient of an authenticated doctor.")
	@GetMapping(value = "/patient/{patientPid}/prescription/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PrescriptionDTO> findPatientPrescriptionsByPatientPid(@PathVariable Long patientPid,
			Pageable pageRequest) throws Exception {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Doctor with id " + doctorId
				+ " is attempting to retrieve prescription history of patient with pid " + patientPid);
		PatientDTO patient = patientService.findByPidDTO(patientPid);
		if (patient == null) {
			throw new NullPointerException("No patient with pid " + patientPid + " exist.");
		} else {
			logger.debug("Patient with pid " + patientPid + " was found.");
		}
		if (patient.getDoctorId().equals(doctorId)) {
			logger.debug("Doctor with id " + doctorId + " is authorized to access details of patient with pid " + patientPid);
			return prescriptionService.findAllByPatientPid(patientPid, pageRequest);
		} else {
			throw new Exception("Unauthorized access:  doctor is not authorized to view details of patient: "
					+ patient.getFirstName() + " " + patient.getLastName() + "with pid " + patient.getPid());
		}
	}

	@ApiOperation(value = " View particular prescription details of one of my patients")
	@GetMapping(value = "/patient/{patientPid}/prescription/{prescriptionId}")
	public PrescriptionDTO findPatientPrescriptionByPatientPidAndPrescriptionId(
			@PathVariable("patientPid") Long patientPid, @PathVariable("prescriptionId") Long prescriptionId)
			throws Exception {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Doctor with id " + doctorId + " is attempting to access prescription with id " + prescriptionId
				+ " belonging to patient with pid " + patientPid);
		PatientDTO patient = patientService.findByPidDTO(patientPid);
		if (patient == null) {
			throw new NullPointerException("No patient with pid " + patientPid + " exists.");
		} else {
			logger.debug("Patient with pid " + patientPid + " was found.");
		}
		if (patient.getDoctorId().equals(doctorId)) {
			logger.debug("Doctor with id " + doctorId + " is authorized to access details of patient with pid " + patientPid);
			return prescriptionService.findByPrescriptionId(prescriptionId);
		} else {
			throw new Exception("Unauthorized access:  doctor is not authorized to view details of patient: "
					+ patient.getFirstName() + " " + patient.getLastName() + "with pid " + patient.getPid());
		}
	}

	@ApiOperation(value = "Create a new prescription", notes = "Creates a new prescription")
	@PostMapping(value = "/new/prescription")
	@ResponseStatus(value = HttpStatus.CREATED)
	public PrescriptionDTO createPrescription(@RequestBody final PrescriptionDTO entityDTO) {
		String doctorUsername = SecurityContextHolder.getContext().getAuthentication().getName();
		Account doctorAccount = accountService.getAccountByUsername(doctorUsername);
		Long doctorId = doctorAccount.getId();
		logger.info("Doctor with id " + doctorId + " is attempting to create a prescription from dto " + entityDTO);
		entityDTO.setDoctorId(doctorId);
		return prescriptionService.createFromDTO(entityDTO);
	}

	@ApiOperation(value = "Create a new medical record", notes = "Creates a new medicalHistory")
	@PostMapping(value = "/new/medical-record")
	@ResponseStatus(value = HttpStatus.CREATED)
	public MedicalHistoryDTO createMedicalRecord(@RequestBody final MedicalHistoryDTO entityDTO) {
		System.out.println("Post called");
		// Ir siaip Facade ir reikalingas sitiems veiksmams atlikti
		String doctorUsername = SecurityContextHolder.getContext().getAuthentication().getName();
		Account doctorAccount = accountService.getAccountByUsername(doctorUsername);
		Long doctorId = doctorAccount.getId();
		logger.info("Doctor with id " + doctorId + " is attempting to create a medical record from dto " + entityDTO);
		entityDTO.setDoctorId(doctorId);
		return medicalHistoryService.createByDTO(entityDTO);
	}

	@ApiOperation(value = "Searches all patients that are patients of authenticated doctor")
	@PostMapping(value = "/patient/search/")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PatientDTO> searchUsersByNameAndSurnameAndPidAndDiagnosis(@RequestBody final PatientSearchDTO searchDTO,
			Pageable pageRequest) {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		searchDTO.setDoctorId(doctorId);
		logger.info("Doctor with id " + doctorId + " is attempting to search his patients using these search parameters: " + searchDTO);
		return patientService.searchByNameAndSurnameAndPidAndDiagnosis(searchDTO, pageRequest);
	}

	@ApiOperation(value = "Finds all prescription fills for patient")
	@PostMapping(value = "/prescriptionFills/{prescriptionId}/search/")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PrescriptionFillsDTO> findFills(@PathVariable("prescriptionId") final Long prescriptionId,
			Pageable pageRequest) throws Exception {
		Long doctorId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Doctor with id " + doctorId + " is attepting to view all prescription fills of prescription with id " + prescriptionId);
		PrescriptionDTO prescriptionFound = prescriptionService.findByPrescriptionId(prescriptionId);
		if (prescriptionFound == null) {
			throw new NullPointerException("No prescription with id " + prescriptionId + " exists.");
		} else {
			logger.debug("Prescription with id " + prescriptionId + " was found.");
		}
		Patient patientFound = patientService.findById(prescriptionFound.getPatientId());
		if (patientFound.getDoctor().getId() == doctorId) {
			logger.info("Doctor with id " + doctorId + " has acces to view details of patient with id " + patientFound.getId());
			return prescriptionFillsService.findByPrescriptionId(prescriptionId, pageRequest);
		} else {
			throw new Exception("Unauthorized access:  doctor is not authorized to view details of patient: "
					+ patientFound.getFirstName() + " " + patientFound.getLastName() + "with id " + patientFound.getId());
		}
		
	}

}
