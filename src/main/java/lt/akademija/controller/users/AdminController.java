package lt.akademija.controller.users;

import io.swagger.annotations.*;
import lt.akademija.model.dto.AccountDTO;
import lt.akademija.model.dto.AccountDTOFull;
import lt.akademija.model.dto.search.BasicUserSearchDTO;
import lt.akademija.model.dto.users.AbstractUserDTO;
import lt.akademija.model.dto.users.PatientDTO;
import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.simple.AbstractSimpleEntity;
import lt.akademija.service.AccountService;
import lt.akademija.service.Facade.AdminFacade;

import javax.security.auth.login.AccountException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

// logger done
// exceptions done
@RestController
@Api(value = "admin")
@RequestMapping(value = "api/admin/")
/**
 *
 * Controller for use by admins
 *
 */
public class AdminController {

	private static final Logger logger = LogManager.getLogger(AdminController.class);

	@Autowired
	private AdminFacade adminFacade;

	@Autowired
	private AccountService accountService;

	@ApiOperation(value = "Enabled/disable user's account.", notes = "Set \"enabled\" to a boolean")
	@PutMapping(value = "/account/{accountId}/{enabled}")
	@ResponseStatus(value = HttpStatus.OK)
	public void setEnabledById(@PathVariable(value = "accountId") Long accountId,
			@PathVariable(value = "enabled") boolean enabled) throws AccountException {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to set account with id " + accountId
				+ " field enabled to " + enabled);
		adminFacade.setEnabledById(accountId, enabled);
	}
	
	@ApiOperation(value = "Reset user's password")
	@PutMapping(value = "/account/{accountId}/reset-password")
	@ResponseStatus(value = HttpStatus.OK)
	public void resetPasswordById(@PathVariable Long accountId) throws AccountException {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to reset password of user with id " + accountId);
		adminFacade.resetPasswordById(accountId);
	}

	@ApiOperation(value = "Given a user type, perform search on firstname, lastname, pid", notes = "Must add \"user\":{user} in JSON where {user} indicates user type in question."
			+ "<br /> To get all users either: "
			+ "<br />    (a) leave all string parameters as empty strin and all long parameters as zeroes "
			+ "<br />    (b): do not include any parameters apart from  \"user\""
			+ "<br /> Parameters can also be passed as a map with keys: 1. firstName, 2. lastName, 3. pid.")
	@PostMapping(value = "/{user-type}/search")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	@ResponseStatus(value = HttpStatus.OK)
	public <T extends AbstractUserDTO> Page<T> searchUsersByNameOrSurnameOrPid(
			@RequestBody final BasicUserSearchDTO searchDTO, @PathVariable(value = "user-type") String user,
			Pageable pageRequest) throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.warn("Admin with id " + adminId + " is attempting to search for " + user + "s with dto " + searchDTO);
		return adminFacade.searchUsersByNameOrSurnameOrPid(user, searchDTO, pageRequest);
	}

	@ApiOperation(value = "View user details of a particular user of given type")
	@GetMapping(value = "/{user}/{userId}")
	@ResponseStatus(value = HttpStatus.OK)
	public <T extends AbstractUserDTO> T getUserById(@PathVariable final String user, @PathVariable final Long userId)
			throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to acces user details of user with id " + " userId");
		return adminFacade.getUserById(user, userId);
	}

	@ApiOperation(value = "Get user account details using user id")
	@GetMapping(value = "/user/account/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public AccountDTOFull getAccountById(@PathVariable("id") final Long userId) throws AccountException {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to acces account details of user with id " + userId);
		return adminFacade.getAccountById(userId);
	}

	@ApiOperation(value = "Create a new user", notes = "Must add \"user\":{user} in JSON")
	@PostMapping(value = "/new/{user}")
	@ResponseStatus(value = HttpStatus.CREATED)
	public <T extends AbstractUserDTO> Long createUser(@PathVariable String user, @RequestBody final T entityDTO,
			@RequestParam("username") String username, @RequestParam("password") String password) throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to create a new user with dto " + entityDTO);
		if (user.trim().equalsIgnoreCase("patient")) {
			logger.debug("Usertype to be created was determined to be patient");
			return adminFacade.createPatient(user, (PatientDTO) entityDTO);
		} else {
			logger.debug("Usertype to be created was determined to be non-patient");
			return adminFacade.createUser(user, entityDTO, username, password);
		}
	}

	@ApiOperation(value = "Create a new simple entity", notes = "Need to include \"entityType\":{entity} in json body")
	@PostMapping(value = "/new/simple/{entity}")
	@ResponseStatus(value = HttpStatus.CREATED)
	public <T extends AbstractSimpleEntity> Long createSimpleEntity(@PathVariable String entity,
			@RequestBody final T entityDTO) throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to create a simple entity with dto " + entityDTO);
		return adminFacade.createSimpleEntity(entity, entityDTO);
	}

	@ApiOperation(value = "Create and associate an account with an existing patient")
	@PostMapping(value = "/new/patient-account/{patientId}")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void createPatientAccount(@PathVariable Long patientId, @RequestBody AccountDTO accountDTO)
			throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to create a patient account with dto " + accountDTO
				+ " for patient with id " + patientId);
		adminFacade.createPatientAccount(patientId, accountDTO);
	}

	@ApiOperation(value = "Update user details", notes = "Must add \"user\":{user} in JSON")
	@PutMapping(value = "/update/{user}/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public <T extends AbstractUserDTO> T updateUserById(@PathVariable("user") final String user,
			@PathVariable("id") final Long id, @RequestBody final T entityDTO) throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info(
				"Admin with id " + adminId + " is attempting to update user with id " + id + " using dto " + entityDTO);
		return adminFacade.updateUserById(user, id, entityDTO);
	}

	@ApiOperation(value = "Update user account details")
	@PutMapping(value = "/update/account/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Account updateUserAccountById(@PathVariable("id") final Long id, @RequestBody AccountDTO accountDTO)
			throws AccountException {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to update account details with id " + id
				+ " using dto " + accountDTO);
		return adminFacade.updateUserAccountById(id, accountDTO);
	}

	@ApiOperation(value = "Update existing simple entity", notes = "Need to include \"entityType\":{entity} in json body")
	@PutMapping(value = "/update/simple/{entity}/{id}")
	@ResponseStatus(value = HttpStatus.CREATED)
	public <T extends AbstractSimpleEntity> T updateSimpleEntity(@PathVariable("entity") String entity,
			@PathVariable("id") Long id, @RequestBody final T entityDTO) throws Exception {
		Long adminId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Admin with id " + adminId + " is attempting to update simple entity with id " + id + " using dto "
				+ entityDTO);
		return adminFacade.updateSimpleEntityById(entity, id, entityDTO);
	}

}
