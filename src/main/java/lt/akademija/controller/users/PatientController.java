package lt.akademija.controller.users;

import io.swagger.annotations.*;
import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.dto.MedicalHistoryPkDTO;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.service.AccountService;
import lt.akademija.service.MedicalHistoryService;
import lt.akademija.service.PrescriptionService;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

// exceptions done
// logging done
@RestController
@Api(value = "patient")
@RequestMapping(value = "api/patient/")
public class PatientController {
	// @InitBinder("patient")
	// protected void initBinder(WebDataBinder binder) {
	// binder.setValidator(new PatientValidator());
	// }

	// @Valid

	private static final Logger logger = LogManager.getLogger(PatientController.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private MedicalHistoryService medicalHistoryService;

	@Autowired
	private PrescriptionService prescriptionService;

	@ApiOperation(value = "Lists all medical records of an authenticated patient", notes = "Returns a list of medical records"
			+ "<br /> Any patient id/pid sent is ignored.")
	@GetMapping(value = "/medical-record/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<MedicalHistoryDTO> findMedicalHistory(Pageable pageRequest) {
		Long patientId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Patient with id " + patientId + " is accessing one's medical history.");
		return medicalHistoryService.findByPatientId(patientId, pageRequest);
	}

	@ApiOperation(value = "View a specific medical record of an authenticated patient", notes = "Any patient Id/pid sent is ignored")
	@PostMapping(value = "/medical-record")
	public MedicalHistoryDTO findMedicalRecordByPK(@RequestBody MedicalHistoryPkDTO dto) throws Exception {
		Long patientId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Patient with id " + patientId + " is accessing one's medical record with PK " + dto);
		Long doctorId = dto.getDoctorId();
		Long diagnosisId = dto.getDiagnosisId();
		Date date = dto.getDate();
		MedicalHistoryDTO recordFound = medicalHistoryService.findByPk(patientId, doctorId, diagnosisId, date);
		if (recordFound == null) {
			throw new NullPointerException("No such medical record found.");
		} else {
			return recordFound;
		}
	}

	@ApiOperation(value = "Lists all prescriptions of an authenticated patient", notes = "Returns a list of prescriptions"
			+ "<br /> Any patient Id/pid sent is ignored")
	@GetMapping(value = "/prescription/all")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Page<PrescriptionDTO> findPrescriptions(Pageable pageRequest) {
		Long patientId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
				.getAbstractUser().getId();
		logger.info("Patient with id " + patientId + " is accessing one's prescription history.");
		return prescriptionService.findAllByPatientId(patientId, pageRequest);
	}

	@ApiOperation(value = "View a specific prescription of authenticated patient")
	@GetMapping(value = "/prescription/{prescriptionId}")
	public PrescriptionDTO findPrescriptionByPrescriptionIdAndPatientId(@PathVariable Long prescriptionId)
			throws Exception {
		Long patientId = accountService
				.getAccountByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
		logger.info("Patient with id " + patientId + " is accessing one's prescription with id " + prescriptionId);
		PrescriptionDTO prescriptionFound = prescriptionService.findByPrescriptionId(prescriptionId);
		if (prescriptionFound == null) {
			throw new NullPointerException(
					"Prescription with id " + prescriptionId + " given does not correspond to any known prescription");
		}
		if (prescriptionFound.getPatientId().equals(patientId)) {
			logger.debug("Prescription with id " + prescriptionId + " belongs to patient with id " + patientId);
			return prescriptionFound;
		} else {
			throw new Exception("Unauthorized access: patient user is trying to access info about another patient");
		}
	}

}
