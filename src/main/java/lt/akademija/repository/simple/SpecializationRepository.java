package lt.akademija.repository.simple;

import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.repository.GenericRepository;

public interface SpecializationRepository extends GenericSimpleEntityRepository<Specialization> {

}
