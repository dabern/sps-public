package lt.akademija.repository.simple;

import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends GenericSimpleEntityRepository<Ingredient> {
}
