package lt.akademija.repository.DTOInterface;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface PrescriptionFillsDTOInterface {
    @Value("#{target.prescriptionFillsPK.timestamp}")
    Date getDate();
    @Value("#{target.prescriptionFillsPK.prescription.id}")
    Long getPrescriptionId();
    @Value("#{target.prescriptionFillsPK.pharmacist.id}")
    Long getPharmacistId();
}
