package lt.akademija.repository.DTOInterface;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface MedicalHistoryDTOInterface {
    @Value("#{target.medicalHistoryPK.patient.id}")
    Long getPatientId();
    @Value("#{target.medicalHistoryPK.patient.pid}")
    Long getPatientPid();
    @Value("#{target.medicalHistoryPK.doctor.id}")
    Long getDoctorId();
    @Value("#{target.medicalHistoryPK.timestamp}")
    Date getDate();
    @Value("#{target.medicalHistoryPK.diagnosis.id}")
    Long getDiagnosisId();
    String getNotes();
    Integer getAppointmentLength();
    Boolean getCompensated();
    @Value("#{target.repeatVisit}")
    Boolean getRepeatVisitation();
}
