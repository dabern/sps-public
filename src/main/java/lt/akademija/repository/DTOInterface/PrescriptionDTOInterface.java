package lt.akademija.repository.DTOInterface;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface PrescriptionDTOInterface {
    Long getId();

    @Value("#{target.activeIngredient.id}")
    Long getActiveIngredient();

    Double getActiveIngredientPerDose();

    String getActiveIngredientUnits();

    String getDosageNotes();

    @Value("#{target.doctor.id}")
    Long getDoctorId();

    @Value("#{target.patient.pid}")
    Long getPatientPid();

    @Value("#{target.patient.id}")
    Long getPatientId();

    Date getPrescriptionDate();

    Date getValidUntil();

}
