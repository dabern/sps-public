package lt.akademija.repository.implementation;

import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.simple.Ingredient;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Repository
public class PrescriptionRepositoryImpl implements RepositoryCustom<PrescriptionDTO> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PrescriptionDTO saveDTO(PrescriptionDTO prescriptionDTO) {
        Long id = prescriptionDTO.getActiveIngredient();
        if(id == null)
        {
            System.out.println("Ingredient id required. Current value null");
            return null;
        }
        Ingredient ingredient = (Ingredient)em.createQuery("Select d from Ingredient d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(ingredient == null)
        {
            System.out.println("Ingredient not found in prescription");
            return null;
        }
        id = prescriptionDTO.getDoctorId();
        if(id == null)
        {
            System.out.println("Doctor id required. Current value null");
            return null;
        }
        Doctor doctor = (Doctor)em.createQuery("Select d from Doctor d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(doctor == null)
        {
            System.out.println("Doctor not found in prescription");
            return null;
        }
        Long pid = prescriptionDTO.getPatientPid();
        if(pid == null)
        {
            System.out.println("Patient pid required. Current value null");
            return null;
        }
        Patient patient = (Patient)em.createQuery("Select d from Patient d where d.pid = ?1")
                .setParameter(1,pid).getSingleResult();
        if(patient == null)
        {
            System.out.println("Patient not found in prescription");
        }
        Prescription prescription = new Prescription(prescriptionDTO, ingredient, doctor, patient);
        this.em.persist(prescription);
        prescriptionDTO.setPrescriptionId(prescription.getId());
        System.out.println(prescription.toString());
        this.em.flush();
        return prescriptionDTO;
    }

    @Override
    public PrescriptionDTO updateDTOByPid(Long pid, PrescriptionDTO DTO) {
        return null;
    }

    @Override
    public PrescriptionDTO updateDTOById(Long id, PrescriptionDTO DTO) {
        return null;
    }
}
