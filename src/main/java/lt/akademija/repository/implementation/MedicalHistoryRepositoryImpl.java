package lt.akademija.repository.implementation;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.users.Doctor;
import lt.akademija.model.entity.users.Patient;
import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Repository
public class MedicalHistoryRepositoryImpl implements RepositoryCustom<MedicalHistoryDTO> {

    @PersistenceContext
    private EntityManager em;


    @Override
    public MedicalHistoryDTO saveDTO(MedicalHistoryDTO historyDTO) {
        Long id = historyDTO.getDiagnosisId();
        if(id == null)
        {
            System.out.println("Diagnosis null passed medical history repo");
            return null;
        }
        Diagnosis diagnosis = (Diagnosis)em.createQuery("Select d from Diagnosis d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        id = historyDTO.getDoctorId();
        if(id == null)
        {
            System.out.println("Doctor id null passed medical history repo");
            return null;
        }
        if(diagnosis == null)
        {
            System.out.println("diagnosis not found");
            return null;
        }
        Doctor doctor = (Doctor)em.createQuery("Select d from Doctor d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(doctor == null)
        {
            System.out.println("doctor not found");
            return null;
        }
        Long pid = historyDTO.getPatientPid();
        if(pid == null)
        {
            System.out.println("Patient pid null passed medical history repo");
            return null;
        }
        Patient patient = (Patient)em.createQuery("Select d from Patient d where d.pid = ?1")
                .setParameter(1,pid).getSingleResult();
        if(patient == null)
        {
            System.out.println("patient not found");
            return null;
        }
        System.out.println("Medical history constructor called: ");
        MedicalHistory history = new MedicalHistory(historyDTO, doctor, patient, diagnosis);
        System.out.println("Medical history: "+history);
        this.em.persist(history);
        this.em.flush();
        return MedicalHistoryDTO.toDTO(history);
    }

    @Override
    public MedicalHistoryDTO updateDTOByPid(Long pid, MedicalHistoryDTO historyDTO) {
        return null;
    }

    @Override
    public MedicalHistoryDTO updateDTOById(Long id, MedicalHistoryDTO historyDTO) {
        return null;
    }
}
