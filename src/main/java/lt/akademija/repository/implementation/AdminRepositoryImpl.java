package lt.akademija.repository.implementation;

import lt.akademija.model.dto.users.AdminDTO;
import lt.akademija.model.entity.users.Admin;
import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Repository
public class AdminRepositoryImpl implements RepositoryCustom<AdminDTO> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public AdminDTO saveDTO(AdminDTO adminDTO) {
        Admin admin = new Admin();
        admin= localCopy(adminDTO, admin);
        this.em.persist(admin);
        adminDTO.setId(admin.getId());
        this.em.flush();
        return adminDTO;
    }

    @Override
    public AdminDTO updateDTOByPid(Long pid, AdminDTO adminDTO) {
        Admin admin = (Admin) em.createQuery("Select d from Admin d where d.pid = ?1")
                .setParameter(1, pid).getSingleResult();
        if(admin == null)
        {
            System.out.println("Admin not found by pid");
            return null;
        }
        admin= localCopy(adminDTO, admin);
        this.em.merge(admin);
        adminDTO.setId(admin.getId());
        this.em.flush();
        return adminDTO;
    }

    @Override
    public AdminDTO updateDTOById(Long id, AdminDTO adminDTO) {
        Admin admin = (Admin) em.createQuery("Select d from Admin d where d.id = ?1")
                .setParameter(1, id).getSingleResult();
        if(admin == null)
        {
            System.out.println("Admin not found by id");
            return null;
        }
        admin= localCopy(adminDTO, admin);
        this.em.merge(admin);
        this.em.flush();
        return adminDTO;
    }

    private Admin localCopy(AdminDTO adminDTO, Admin admin) {
        admin.setPid(adminDTO.getPid());
        admin.setFirstName(adminDTO.getFirstName());
        admin.setLastName(adminDTO.getLastName());
        return admin;
    }
}
