package lt.akademija.repository.implementation;

import lt.akademija.model.dto.PrescriptionFillsDTO;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.PrescriptionFills;
import lt.akademija.model.entity.PrescriptionFillsPK;
import lt.akademija.model.entity.users.Pharmacist;
import lt.akademija.repository.users.RepositoryCustom;
import lt.akademija.service.PrescriptionFillsService;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;

@Transactional
@Repository
public class PrescriptionFillsRepositoryImpl implements RepositoryCustom<PrescriptionFillsDTO> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PrescriptionFillsDTO saveDTO(PrescriptionFillsDTO prescriptionFillsDTO) {
        Long id = prescriptionFillsDTO.getPhramacistId();
        if(id == null)
        {
            System.out.println("In PrescriptionFillsRepo pharmacistId null passed");
            return null;
        }
        Pharmacist pharmacist = (Pharmacist) em.createQuery("Select d from Pharmacist d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        if(pharmacist == null)
        {
            System.out.println("Pharmacist not found in prescription fills");
            return null;
        }
        id = prescriptionFillsDTO.getPrescriptionId();
        if(id == null)
        {
            System.out.println("In PrescriptionFillsRepo prescriptionid null passed");
            return null;
        }
        Prescription prescription = (Prescription) em.createQuery("Select d from Prescription d where d.id = ?1")
                .setParameter(1,id).getSingleResult();
        Date date = prescriptionFillsDTO.getDate();
        PrescriptionFills prescriptionFills = new PrescriptionFills(date, pharmacist, prescription);
        this.em.persist(prescriptionFills);
        this.em.flush();
        return PrescriptionFillsDTO.toDTO(prescriptionFills);
    }

    @Override
    public PrescriptionFillsDTO updateDTOByPid(Long pid, PrescriptionFillsDTO DTO) {
        return null;
    }

    @Override
    public PrescriptionFillsDTO updateDTOById(Long id, PrescriptionFillsDTO DTO) {
        return null;
    }
}
