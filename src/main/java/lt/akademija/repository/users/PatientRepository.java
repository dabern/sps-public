package lt.akademija.repository.users;
import lt.akademija.model.entity.MedicalHistory;
import lt.akademija.model.entity.Prescription;
import lt.akademija.model.entity.users.Patient;

import lt.akademija.model.dto.users.PatientDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface PatientRepository extends GenericUserRepository<Patient>, RepositoryCustom<PatientDTO> {

	Page<Patient> findAllByOrderByDobAsc(Pageable pageRequest);

    @Query(value = "Select m from Patient p JOIN p.medicalHistories m where m.medicalHistoryPK.patient.pid = ?1")
    Page<MedicalHistory> findMedicalRecordsByPid(Pageable pageRequest, Long pid);

    @Query(value = "Select pr from Patient p JOIN p.prescriptions pr where pr.patient.pid = ?1")
    Page<Prescription> findPrescriptionListByPid(Pageable pageRequest, Long pid);

    @Query(value = "Select p from Patient p JOIN p.doctor d where d.pid = ?1")
    Page<Patient> findPatientListByPid(Long pid, Pageable pageable);

    @Query(value = "Select p from Patient p JOIN p.doctor d where d.id = ?1")
    Page<Patient> findPatientListByDoctorId(Long id, Pageable pageable);

    @Query(value= "select distinct p from Patient p LEFT OUTER JOIN p.medicalHistories m LEFT OUTER JOIN m.medicalHistoryPK.diagnosis d"
            +" where p.doctor.id = (:doctorId)"
            +"AND UPPER(p.firstName) LIKE UPPER(:firstName)||'%'"
            +"AND UPPER(p.lastName) LIKE UPPER(:lastName)||'%'"
            +"AND CAST(p.pid as text) LIKE (:pid)||'%'"
            +"AND d.title = :title)")
    Page<Patient> searchByNameAndSurnameAndPidAndDiagnosis(@Param(value = "firstName") String firstName,
                                       @Param(value = "lastName") String lastName,
                                       @Param(value = "pid") String pid,
                                       @Param(value = "title") String title,
                                       @Param(value = "doctorId") Long id, Pageable pageRequest);

    @Query(value= "select distinct p from Patient p LEFT OUTER JOIN p.medicalHistories m"
            +" where p.doctor.id = (:doctorId)"
            +"AND UPPER(p.firstName) LIKE UPPER(:firstName)||'%'"
            +"AND UPPER(p.lastName) LIKE UPPER(:lastName)||'%'"
            +"AND CAST(p.pid as text) LIKE (:pid)||'%'")
    Page<Patient> searchByNameAndSurnameAndPid(@Param(value = "firstName") String firstName,
                                             @Param(value = "lastName") String lastName,
                                             @Param(value = "pid") String pid,
                                             @Param(value = "doctorId") Long id, Pageable pageRequest);

//    //Find by FirstName
//    Page<Patient> findAllByDoctor_idAndFirstNameStartingWithIgnoreCase(Long id, String firstName, Pageable pageRequest);
//
//    //Find by LastName
//    List<Patient> findAllByDoctor_idAndLastNameStartingWithIgnoreCase(Long id, String lastName);
//
//    //Finds a patient by PID if he belongs to doctor
//    Patient findByDoctor_idAndPid(Long id, Long Pid);

    //Query patients by disease code
    //Fetch needed for lazy.FETCH members
    @Query(name = "FindByDisease",
            value = "Select p from Patient p JOIN FETCH p.medicalHistories m JOIN FETCH m.medicalHistoryPK.diagnosis d where d.title = :code")
    List<Patient> findAllByDisease(@Param("code") String code);
}
