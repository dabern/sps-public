package lt.akademija.repository.users;

import lt.akademija.model.dto.users.AdminDTO;
import lt.akademija.model.entity.users.Admin;

import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends GenericUserRepository<Admin>, RepositoryCustom<AdminDTO> {

}
