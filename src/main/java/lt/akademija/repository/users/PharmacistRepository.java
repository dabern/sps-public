package lt.akademija.repository.users;

import lt.akademija.model.dto.users.PharmacistDTO;
import lt.akademija.model.entity.users.Pharmacist;
import org.springframework.stereotype.Repository;

@Repository
public interface PharmacistRepository extends GenericUserRepository<Pharmacist>, RepositoryCustom<PharmacistDTO> {

}
