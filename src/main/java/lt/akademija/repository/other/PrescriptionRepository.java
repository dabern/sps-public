package lt.akademija.repository.other;

import lt.akademija.repository.GenericRepository;
import lt.akademija.repository.DTOInterface.PrescriptionDTOInterface;
import lt.akademija.repository.users.RepositoryCustom;
import lt.akademija.model.dto.PrescriptionDTO;
import lt.akademija.model.entity.Prescription;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionRepository extends GenericRepository<Prescription>, RepositoryCustom<PrescriptionDTO> {
   // List<Prescription> findOneByPatient_Pid(Long Pid);

   // @Query("select p from Prescription p JOIN p.patient pt where pt.pid = ?1")
    Page<PrescriptionDTOInterface> findAllByPatient_Pid(Long pid, Pageable pageRequest);

    Page<PrescriptionDTOInterface> findAllByPatient_Id(Long id, Pageable pageRequest);

    PrescriptionDTOInterface findOneById(Long prescriptionId);
}
