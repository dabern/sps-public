package lt.akademija.repository.other;

import lt.akademija.model.dto.MedicalHistoryDTO;
import lt.akademija.model.entity.*;
import lt.akademija.repository.GenericRepository;
import lt.akademija.repository.DTOInterface.MedicalHistoryDTOInterface;

import java.util.Date;

import lt.akademija.repository.users.RepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalHistoryRepository extends GenericRepository<MedicalHistory>, RepositoryCustom<MedicalHistoryDTO>{

   // @Query("select mh from MedicalHistory mh JOIN FETCH mh.medicalHistoryPK.patient p where p.pid = ?1 ")
    Page<MedicalHistoryDTOInterface> findByMedicalHistoryPK_Patient_Pid/*OrderByMedicalHistoryPK_TimestampDesc*/(Long pid, Pageable pageRequest);

    Page<MedicalHistoryDTOInterface> findAllByMedicalHistoryPK_Patient_Id/*OrderByMedicalHistoryPK_TimestampDesc*/(Long id, Pageable pageRequest);

    //@Query
	MedicalHistoryDTOInterface findByMedicalHistoryPK_Patient_IdAndMedicalHistoryPK_Doctor_IdAndMedicalHistoryPK_Diagnosis_IdAndMedicalHistoryPK_Timestamp(
			Long patientId, Long doctorId, Long diagnosisId, Date date);

//	MedicalHistoryDTOInterface findDistinctByMedicalHistoryPK_Patient_Id(Long id);

//	 @Query("Select m.medicalHistoryPK.Doctor from MedicalHistory m JOIN FETCH
//	 m.medicalHistoryPK.doctor d where d.id = :id")
//	 Doctor findDoctor(@Param("id") Long id);

	// @Query("Select m.medicalHistoryPK.Patient from MedicalHistory m JOIN FETCH
	// m.medicalHistoryPK.patient d where d.id = :id")
	// Patient findPatient(@Param("id") Long id);
	//
	// @Query("Select m.medicalHistoryPK.diagnosis from Diagnosis m where
	// m.medicalHistoryPK.diagnosis.id = :id")
	// Diagnosis findDiagnosis(@Param("id") Long id);
}
