

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import lt.akademija.model.entity.users.Pharmacist;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import lt.akademija.model.entity.Account;
import lt.akademija.model.entity.users.Admin;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = lt.akademija.App.class)
@AutoConfigureMockMvc
public class AdminWithMockTest {

	@Autowired
	private MockMvc mvc;

	ObjectMapper mapper = new ObjectMapper();
/*create users test*/	
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testCreateAdmin() throws Exception {

        String admin="{"
        		+ "\"user\": \"admin\","
        		+ "\"firstName\": \"TestName\","
        		+ "\"id\": 2,"
        		+ "\"lastName\": \"LastName\","
        		+ "\"pid\":42209198201"
        		+ "}";
        
        mvc.perform(post("/api/admin/new/admin?username=admin33&password=admin33")
                .contentType(MediaType.APPLICATION_JSON)
                .content(admin))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testCreatePharmacist() throws Exception {

        String pharmacist="{\"user\": \"pharmacist\","
        		+ "\"id\":10,"
        		+ "\"firstName\": \"TestName\","
        		+ "\"lastName\": \"LastName\","
        		+ "\"pid\": 35702238138,"
        		+ "\"organization\": \"TestPharmacy\""
        		+ "}";
        
        mvc.perform(post("/api/admin/new/pharmacist?username=pharmacist01&password=pharmacist01")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pharmacist))
                .andExpect(status().isCreated());
    }
    
    
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testCreateDoctor() throws Exception {

        String doctor="{\"user\": \"doctor\","
        		+ "\"id\":20,"
        		+ "\"firstName\": \"TestName\","
        		+ "\"lastName\": \"LastName\","
        		+ "\"pid\": 35702238138,"
        		+ "\"specializatioId\": 2"
        		+ "}";
        
        mvc.perform(post("/api/admin/new/doctor?username=doctor01&password=doctor01")
                .contentType(MediaType.APPLICATION_JSON)
                .content(doctor))
                .andExpect(status().isCreated());
    }
/*edit users*/    
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testEditDoctor() throws Exception {
    	
    	String doctor="{\"user\": \"doctor\","
        		+ "\"id\":2021,"
        		+ "\"firstName\": \"EditedName\","
        		+ "\"lastName\": \"LastName\","
        		+ "\"pid\": 44502179108,"
        		+ "\"specializatioId\": 2"
        		+ "}";
        
        mvc.perform(put("/api/admin/update/doctor/2021")
                .contentType(MediaType.APPLICATION_JSON)
                .content(doctor))
                .andExpect(status().isOk());
        
        mvc.perform(MockMvcRequestBuilders.get("/api/admin/doctor/2021")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
                .andExpect(content().string(containsString("EditedName")));
    	
    }
    
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testEditPharmacist() throws Exception {
    	  String pharmacist="{\"user\": \"pharmacist\","
          		+ "\"id\":201,"
          		+ "\"firstName\": \"EditedName\","
          		+ "\"lastName\": \"LastName\","
          		+ "\"pid\": 61309159925,"
          		+ "\"organization\": \"TestPharmacy\""
          		+ "}";
    	  
    	  mvc.perform(put("/api/admin/update/pharmacist/201")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(pharmacist))
                  .andExpect(status().isOk());
          
    	
    	mvc.perform(MockMvcRequestBuilders.get("/api/admin/pharmacist/201")
					.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk())
					.andExpect(content().string(containsString("EditedName")));
    }
    
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testEditPatient() throws Exception {
    	
    	String patient="{\"user\": \"patient\","
        		+ "\"id\":6600,"
        		+ "\"firstName\": \"EditedName\","
        		+ "\"lastName\": \"LastName\","
        		+ "\"pid\": 44709121040,"
        		+ "\"dob\": \"1947-09-12\","
        		+ "\"doctorId\": 3170,"
        		+ "\"doctorPid\": 45211264398"
        		+ "}";
    	
    	mvc.perform(put("/api/admin/update/patient/6600")
                .contentType(MediaType.APPLICATION_JSON)
                .content(patient))
                .andExpect(status().isOk());
  	
  	mvc.perform(MockMvcRequestBuilders.get("/api/admin/patient/6600")
					.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk())
					.andExpect(content().string(containsString("EditedName")));
    	
    }
    
/*find users test*/    
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testFindUserByUserTypeAndId() throws Exception {
    			 mvc.perform(MockMvcRequestBuilders.get("/api/admin/doctor/2200")
    						.accept(MediaType.APPLICATION_JSON))
    						.andExpect(status().isOk())
    						.andExpect(content().string(allOf(
    								containsString("id"),
    								containsString("firstName"))));
    			 
    			 mvc.perform(MockMvcRequestBuilders.get("/api/admin/patient/6200")
 						.accept(MediaType.APPLICATION_JSON))
 						.andExpect(status().isOk())
 						.andExpect(content().string(allOf(
 								containsString("id"),
 								containsString("firstName"))));
    			 
    			 mvc.perform(MockMvcRequestBuilders.get("/api/admin/pharmacist/200")
  						.accept(MediaType.APPLICATION_JSON))
  						.andExpect(status().isOk())
  						.andExpect(content().string(allOf(
  								containsString("id"),
  								containsString("firstName"))));
    }
    
    /*find users by last name, first name, pid*/
    @Test
    @WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
    public void testFindUserByDTO() throws Exception {
    	
    	String searchByFirstName="{\"user\": \"patient\",\"firstName\": \"Adell\"}";
    	String searchByLastName="{\"lastName\": \"Abbott\"}";
    	String searchByPid="{\"pid\": 42802265614}";
    
	 mvc.perform(post("/api/admin/patient/search")
			 	.contentType(MediaType.APPLICATION_JSON)
			 	.accept(MediaType.APPLICATION_JSON)
				.content(searchByLastName))
				.andExpect(status().isOk())				
				.andExpect(content().string(containsString("Abbott")));
	 
	 mvc.perform(post("/api/admin/patient/search")
			 	.contentType(MediaType.APPLICATION_JSON)
			 	.accept(MediaType.APPLICATION_JSON)
				.content(searchByFirstName))
				.andExpect(status().isOk())				
				.andExpect(content().string(containsString("Adell")));
	 
	 mvc.perform(post("/api/admin/patient/search")
			 	.contentType(MediaType.APPLICATION_JSON)
			 	.accept(MediaType.APPLICATION_JSON)
				.content(searchByPid))
				.andExpect(status().isOk())				
				.andExpect(content().string(containsString("Adell")));
	 
    }
//
///*  _1_  test admin is created and found */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testCreateAdmin() throws Exception {
//
//	    Admin admin = new Admin();
//	    admin.setPid(42212019793L);
//
////	    {
////	    	{\"appointmentLength\": 10,	    	  "compensated": true,
////	    	  "date": "2018-02-13T14:22:44.516Z",
////	    	  "diagnosisId": 0,
////	    	  "doctorId": 0,
////	    	  "notes": "string",
////	    	  "patientId": 0,
////	    	  "repeatVisitation": true
////	    	}
//
//	    mvc.perform(post("/admin/new")
//	                    .contentType(MediaType.APPLICATION_JSON)
//	                    .content(mapper.writeValueAsString(admin)))
//	            .andExpect(status().isCreated());
//
//
///* _2_ test get admin by Id */
//
//		 mvc.perform(MockMvcRequestBuilders.get("/admin/1")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						containsString("id"),
//						containsString("pid"))))
//				.andExpect(content().json("{\"id\": 1,\"pid\": 42212019793}"));
//
//
///*  _3_  test admin's update by ID*/
//
//		mvc.perform(put("/admin/1/update")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content("{\"id\": 1,\"pid\": 40909308386}"))
//				.andExpect(status().isOk());
//
//		mvc.perform(MockMvcRequestBuilders.get("/admin/1")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().json(
//						"{\"id\": 1,\"pid\": 40909308386}"));
//	}
}