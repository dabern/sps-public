//
//import static org.hamcrest.CoreMatchers.allOf;
//import static org.hamcrest.CoreMatchers.containsString;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import lt.akademija.model.entity.users.Pharmacist;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = lt.akademija.App.class)
//@AutoConfigureMockMvc
//public class PharmacistTestWithMock {
//
//	@Autowired
//	private MockMvc mvc;
//
//	ObjectMapper mapper = new ObjectMapper();
//
///*  _1_  test pharmacist's creation*/
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testCreatePharmacist() throws Exception {
//
//	    Pharmacist pharm = new Pharmacist();
//	    pharm.setFirstName("TestName");
//	    pharm.setLastName("TestSurname");
//	    pharm.setOrganization("organization");
//	    pharm.setPid(46601298634L);
//	    //pharm.setType(Pharmacist.Type.AB);
//
//	    mvc.perform(post("/api/pharmacist/new")
//	                    .contentType(MediaType.APPLICATION_JSON)
//	                    .content(mapper.writeValueAsString(pharm)))
//	            .andExpect(status().isCreated());
//	}
//
///* _2_ test get pharmacist by Id */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetPharmacistById() throws Exception {
//
//		 mvc.perform(MockMvcRequestBuilders.get("/api/pharmacist/id/103")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("organization"))));
//	}
//
///*  _3_  test pharmacist's update by ID*/
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testPharmacistUpdateById() throws Exception {
//
//		mvc.perform(put("/api/pharmacist/id/102/update")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content("{\n" +
//						"  \"firstName\": \"UpdatedName\",\n" +
//						"  \"lastName\": \"LastName\",\n" +
//						"  \"organization\": \"string\",\n" +
//						"  \"organizationType\": \"string\",\n" +
//						"  \"pid\": 50808085512\n" +
//						"}"))
//				.andExpect(status().isOk());
//
//		mvc.perform(MockMvcRequestBuilders.get("/api/pharmacist/id/102")
//				.accept(MediaType.APPLICATION_JSON))
//		.andExpect(status().isOk())
//		.andExpect(content().string(allOf(
//				containsString("pid"),
//				containsString("firstName"),
//				containsString("LastName"),
//				containsString("UpdatedName")
//				)));
//	}
//
//	/* _2_ test get pharmacist by PID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetPharmacistByPid() throws Exception {
//
//		 mvc.perform(MockMvcRequestBuilders.get("/api/pharmacist/pid/50808085512")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("organization"))));
//	}
//
///*  _3_  test pharmacist's update by PID*/
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testPharmacistUpdateByPid() throws Exception {
//
//		mvc.perform(put("/api/pharmacist/pid/50808085512/update")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content("{\n" +
//						"      \"firstName\": \"UpdatedName\",\n" +
//						"      \"lastName\": \"LastName\",\n" +
//						"      \"pid\": 50808085512,\n" +
//						"      \"organizationType\": null,\n" +
//						"      \"organization\": \"VšĮ Fédération Internationale Féline\"\n" +
//						"    },"))
//				.andExpect(status().isOk());
//
//		mvc.perform(MockMvcRequestBuilders.get("/api/pharmacist/pid/50808085512")
//				.accept(MediaType.APPLICATION_JSON))
//		.andExpect(status().isOk())
//		.andExpect(content().string(allOf(
//				containsString("pid"),
//				containsString("firstName"),
//				containsString("lastName"),
//				containsString("UpdatedName")
//				)));
//	}
//}
