//import static org.hamcrest.CoreMatchers.allOf;
//import static org.hamcrest.CoreMatchers.containsString;
//import static org.hamcrest.CoreMatchers.equalTo;
//import static org.hamcrest.CoreMatchers.is;
//import static org.junit.Assert.assertThat;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import lt.akademija.model.entity.users.Doctor;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = lt.akademija.App.class)
//@AutoConfigureMockMvc
//public class DoctorTestWithMock {
//
//	@Autowired
//	private MockMvc mvc;
//
//	ObjectMapper mapper = new ObjectMapper();
//
//	/* _1_ test get all doctors */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetDoctorsAll() throws Exception {
//
//		MvcResult result = mvc.perform(get("/api/doctor/all")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						// containsString("id"),
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("specializationID"))))
//				.andReturn();
//	}
//
//	/* _2_ test doctor's creation */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testCreateDoctor() throws Exception {
//
//		mvc.perform(post("/api/doctor/new")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(
//				"{\"firstName\": \"TestName\","
//				+ "\"lastName\": \"Lastname\", "
//				+ "\"pid\": 11111111118,"
//				+ "\"specializationID\": 5}"))
//				.andExpect(status().isCreated());
//	}
//
//
//	/* _4_ test get doctor by PID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetDoctorByPID() throws Exception {
//
//		MvcResult result = mvc.perform(get("/api/doctor/pid/34412293113")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						// containsString("id"),
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("specializationID"))))
//				.andReturn();
//	}
//	/* _5_ test get doctor by ID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetDoctorByID() throws Exception {
//
//		MvcResult result = mvc.perform(get("/api/doctor/id/2000")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						// containsString("id"),
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("specializationID"))))
//				.andReturn();
//	}
//
//	/* _6_ get all doctor's patients by doctor PID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetDoctorsPatients() throws Exception {
//
//	 mvc.perform(get("/api/doctor/34412293113/Patients")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//	}
//
//	/* _7_ test update doctor by PID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testDoctorUpdateByPID() throws Exception {
//
//			mvc.perform(put("/api/doctor/pid/47306084220/update")
//					.contentType(MediaType.APPLICATION_JSON)
//					.content("{"
//							+ "\"firstName\": \"UpdatedName\","
//							+ "\"lastName\": \"TestUpdated\","
//							+ "\"pid\":47306084220,"
//							+ "\"specializationID\": 1}"))
//					.andExpect(status().isOk());
//
//			mvc.perform(get("/api/doctor/pid/47306084220")
//					.accept(MediaType.APPLICATION_JSON))
//					.andExpect(status().isOk())
//					.andExpect(content().string(allOf(
//							containsString("pid"),
//							containsString("firstName"),
//							containsString("lastName"),
//							containsString("UpdatedName")
//							)));
//	}
//	/* _8_ test update doctor by ID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testDoctorUpdateByID() throws Exception {
//
//			mvc.perform(put("/api/doctor/id/2001/update")
//					.contentType(MediaType.APPLICATION_JSON)
//					.content("{"
//							+ "\"firstName\": \"UpdatedName\","
//							+ "\"lastName\": \"TestUpdated\","
//							+ "\"pid\":47306084221,"
//							+ "\"specializationID\": 1}"))
//					.andExpect(status().isOk());
//
//			mvc.perform(get("/api/doctor/id/2001")
//					.accept(MediaType.APPLICATION_JSON))
//					.andExpect(status().isOk())
//					.andExpect(content().string(allOf(
//							containsString("pid"),
//							containsString("firstName"),
//							containsString("lastName"),
//							containsString("UpdatedName")
//							)));
//	}
//	/* _9_ test get doctor by Lastname */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetDoctorByLastname() throws Exception {
//
//		MvcResult result = mvc.perform(get("/api/doctor/searchByLastName/Kertzmann")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						// containsString("id"),
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("specializationID"))))
//				.andReturn();
//	}
//}