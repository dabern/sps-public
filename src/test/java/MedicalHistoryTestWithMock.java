//
//
//import static org.hamcrest.CoreMatchers.allOf;
//import static org.hamcrest.CoreMatchers.containsString;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import lt.akademija.model.entity.MedicalHistory;
//import lt.akademija.model.entity.MedicalHistoryPK;
//import lt.akademija.model.entity.users.Admin;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = lt.akademija.App.class)
//@AutoConfigureMockMvc
//public class MedicalHistoryTestWithMock {
//
//	@Autowired
//	private MockMvc mvc;
//
//	ObjectMapper mapper = new ObjectMapper();
//
///*  _1_  test medicalRecord is created  */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testMedicalRecordCreation() throws Exception {
//
//
//	    mvc.perform(post("/api/medicalRecords/new")
//	                    .contentType(MediaType.APPLICATION_JSON)
//	                    .content("{\"appointmentLength\": 10,\"compensated\": true,\"date\": \"2018-02-13T14:26:27.648Z\",\"diagnosisId\": 1,\"doctorPid\": 44444444444,\"notes\": \"nothing serious\",\"patientPid\": 11111111111,\"repeatVisitation\": true}"))
//	            .andExpect(status().isCreated());
//	}
//
///* _2_ test get all medical record by patient's Id */
//	    @Test
//		@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//		public void testGetMedicalRecordByPID() throws Exception {
//		 mvc.perform(MockMvcRequestBuilders.get("/api/medicalRecords/11111111111")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//	    }
//
///*  _3_  test get all medical records*/
//		 	@Test
//			@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//			public void testGetAllMedicalRecord() throws Exception {
//
//		mvc.perform(MockMvcRequestBuilders.get("/api/medicalRecords/all")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//
//	}
//}