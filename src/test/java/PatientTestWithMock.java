//
//import static org.hamcrest.CoreMatchers.allOf;
//import static org.hamcrest.CoreMatchers.both;
//import static org.hamcrest.CoreMatchers.containsString;
//import static org.hamcrest.CoreMatchers.equalTo;
//import static org.hamcrest.CoreMatchers.is;
//import static org.junit.Assert.assertThat;
//import static org.hamcrest.CoreMatchers.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.json.JacksonJsonParser;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.security.web.FilterChainProxy;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import lt.akademija.model.entity.users.Patient;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = lt.akademija.App.class)
//@AutoConfigureMockMvc
////@WebAppConfiguration
//public class PatientTestWithMock {
//
//	@Autowired
//	private MockMvc mvc;
//
////    @Autowired
////   private WebApplicationContext wac;
////
////    @Autowired
////    private FilterChainProxy springSecurityFilterChain;
////
////	 @Before
////	    public void setup() {
////	        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac)
////	          .addFilter(springSecurityFilterChain).build();
////	    }
////
//// private String obtainAccessToken(String username, String password) throws Exception {
////
////		    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
////		    params.add("grant_type", "password");
////		    params.add("client_id", "fooClientIdPassword");
////		    params.add("username", username);
////		    params.add("password", password);
////
////		    ResultActions result
////		      = mvc.perform(post("/oauth/token")
////		        .params(params)
////		        .with(httpBasic("fooClientIdPassword","secret"))
////		        .accept("application/json;charset=UTF-8"))
////		        .andExpect(status().isOk())
////		        .andExpect(content().contentType("application/json;charset=UTF-8"));
////
////		    String resultString = result.andReturn().getResponse().getContentAsString();
////
////		    JacksonJsonParser jsonParser = new JacksonJsonParser();
////		    return jsonParser.parseMap(resultString).get("access_token").toString();
////		}
//
//	ObjectMapper mapper = new ObjectMapper();
//
///* _1_ test get all patients */
//
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetAllPatients() throws Exception {
//		 mvc.perform(MockMvcRequestBuilders.get("/api/patient/all").accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("dob"))));
//	}
//
//	/* _2_ test patient creation*/
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testCreatePatient() throws Exception {
//
//		mvc.perform(post("/api/patient/new")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content("{"
//						+ "\"dob\": \"1904-10-09\","
//						+ "\"doctorId\": 1,"
//						+ "\"firstName\": \"Test\","
//						+ "\"lastName\": \"Test\","
//						+ "\"pid\":11111111111}"))
//				.andExpect(status().isCreated());
//	}
//
///* _3_ test get patient by PID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetPatientByPid() throws Exception {
//
//		/* check if patient with all required fields are returned  and optionally - are filled correctly*/
//		MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/api/patient/pid/48303162184")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						//containsString("id"),
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("dob"))))
//
////				/*optional start*/
////				.andExpect(content().json(
////						"{\"id\":1,\"pid\":51408041781,\"firstName\":\"Omer\",\"lastName\":\"Schamberger\",\"dob\":\"2014-08-04\"}"))
////				/*optional end */
//				.andReturn();
//
//
//		/* convert Mvc result to patient class object just4fun*/
//		Patient patient = null;
//		try {
//			patient = mapper.readValue(result.getResponse().getContentAsString(), Patient.class);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		//assertThat(patient.getFirstName(), is("Isadore"));
//	}
//
//	/* _4_ test get patient by ID */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetPatientById() throws Exception {
//
//	mvc.perform(MockMvcRequestBuilders.get("/api/patient/id/6102")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						//containsString("id"),
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("dob"))));
//
//	}
///* _5_  test get patient's medical history */
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//	public void testGetPatienMedicalHistory() throws Exception{
//		MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/api/patient/35302079294/medicalRecords").accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andReturn();
//	}
//
///*  _6_  test patient update by PID*/
//	@Test
//	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//	public void testPatientUpdateByPid() throws Exception {
//
//		mvc.perform(put("/api/patient/pid/45610083224/update")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content("{"
//						+ "\"dob\": \"1904-10-09\","
//						+ "\"doctorPid\": 38706254976,"
//						+ "\"firstName\": \"UpdatedName\","
//						+ "\"lastName\": \"TestUpdated\","
//						+ "\"pid\":45610083224}"))
//				.andExpect(status().isOk());
//
//		mvc.perform(MockMvcRequestBuilders.get("/api/patient/pid/45610083224")
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().string(allOf(
//						containsString("pid"),
//						containsString("firstName"),
//						containsString("lastName"),
//						containsString("dob"),
//						containsString("UpdatedName")
//						)));
//	}
//		/*  _7_  test patient update by ID*/
//		@Test
//		@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
//		public void testPatientUpdateById() throws Exception {
//
//			mvc.perform(put("/api/patient/id/6103/update")
//					.contentType(MediaType.APPLICATION_JSON)
//					.content("{"
//							+ "\"dob\": \"1968-02-29\","
//							+ "\"doctorPid\": 38706254976,"
//							+ "\"firstName\": \"UpdatedName\","
//							+ "\"lastName\": \"TestUpdated\","
//							+ "\"pid\":46802296499}"))
//					.andExpect(status().isOk());
//
//			mvc.perform(MockMvcRequestBuilders.get("/api/patient/id/6103/")
//					.accept(MediaType.APPLICATION_JSON))
//					.andExpect(status().isOk())
//					.andExpect(content().string(allOf(
//							containsString("pid"),
//							containsString("firstName"),
//							containsString("lastName"),
//							containsString("dob"),
//							containsString("UpdatedName")
//							)));
//		}
//
//
//		/* _4_ test get patient by LastName */
//		@Test
//		@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "SUPERADMIN" })
//		public void testGetPatientByLastname() throws Exception {
//
//		mvc.perform(MockMvcRequestBuilders.get("/api/patient/searchByLastName/Price")
//					.accept(MediaType.APPLICATION_JSON))
//					.andExpect(status().isOk())
//					.andExpect(content().string(allOf(
//							//containsString("id"),
//							containsString("pid"),
//							containsString("firstName"),
//							containsString("lastName"),
//							containsString("dob"))));
//
//		}
//
//	}
//
////      /*other variant*/
////		/*get some patient data*/
////		MvcResult result1 =mvc.perform(get("/api/patient/35302079294")
////				.accept(MediaType.APPLICATION_JSON))
////				.andExpect(status().isOk())
////				.andReturn();
////
////		String patientData=result1.getResponse().getContentAsString();
////
////
////		/*convert data to Doctor.class object*/
////		Patient patient1 = mapper.readValue(result1.getResponse().getContentAsString(), Patient.class);
////
////		/*Change name in Doctor.class object*/
////		patient1.setFirstName("UpdatedName");
////
////		/*put updated patient data to DB */
////
////		mvc.perform(put("/api/patient/35302079294/update")
////				.contentType(MediaType.APPLICATION_JSON)
////				.content(mapper.writeValueAsString(patient1)))
////				.andExpect(status().isOk());
////
////		/*get updated data of  patient form DB*/
////
////		MvcResult result2 =mvc.perform(get("api/patient/35302079294")
////				.accept(MediaType.APPLICATION_JSON))
////				.andExpect(status().isOk())
////				.andReturn();
////		/*Convert data to Doctor.class object*/
////		Patient patient2 = mapper.readValue(result2.getResponse().getContentAsString(), Patient.class);
////
////		/*check if required data is updated*/
////		assertThat(patient2.getFirstName(), is("UpdatedName"));
////		assertThat(patient2.getLastName(), is(equalTo(patient1.getLastName())));
//
////
/////* _6_  test find patient doctor ID and patient's PID */
////	@Test
////	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
////	public void testFindDoctorsPatientByPid() throws Exception {
////
////		MvcResult result=mvc.perform(get("/patient/1/51408041781")
////				.contentType(MediaType.APPLICATION_JSON))
////				.andExpect(status().isOk())
////				.andExpect(content().string(both(containsString("Omer")).and(containsString ("Schamberger"))))
////				.andReturn();
////	}
////
/////* _7_   test find patient doctor ID and patient's LastName */
////	@Test
////	@WithMockUser(password = "sa", value = "user", username = "sa", roles = { "ADMIN" })
////	public void testFindDoctorsPatientByLastName() throws Exception {
////
////		mvc.perform(get("/patient/SurnameSearch/1/Schamberger")
////				.contentType(MediaType.APPLICATION_JSON))
////				.andExpect(status().isOk())
////				.andExpect(content().string(both(containsString("Omer")).and(containsString ("Schamberger"))));
////	}
////}
