package lt.akademija.service.simple;

import lt.akademija.model.entity.simple.Diagnosis;
import lt.akademija.model.entity.simple.Specialization;
import lt.akademija.repository.simple.DiagnosisRepository;
import lt.akademija.repository.simple.SpecializationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.data.domain.Page;
import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = lt.akademija.App.class)
public class DiagnosisServiceTest {

    @TestConfiguration
    static class DiagnosisServiceTestContextConfiguration{
        @Bean
        DiagnosisService getDiagnosisService(){
            return new DiagnosisService();
        }
    }

    @Before
    public void setUp() {
        Diagnosis  diagnosis = new Diagnosis();
        Long id = new Long(1);
        String title = "TestTitle";
        diagnosis.setDescription("TestDescription");
        diagnosis.setTitle(title);
        diagnosis.setId(id);
        PageRequest pageRequest = new PageRequest(1, 10, Sort.Direction.ASC, "title", "description");
        Page result = new PageImpl(Arrays.asList(diagnosis));

        Mockito.when(genericRepository.findAll(pageRequest))
                .thenReturn(result);
        Mockito.when(genericRepository.findOne(id))
                .thenReturn(diagnosis);
        Mockito.when(genericRepository.findByTitle(title))
                .thenReturn(diagnosis);
        Mockito.when(genericRepository.save(diagnosis))
                .thenReturn(diagnosis);
    }

    @MockBean
    private DiagnosisRepository genericRepository;

    @Autowired
    private DiagnosisService service;

    //1 test, tu get list of diagnosis;
    @Test
    public void whenFindAll_PageWithDiagnosisFound() {
        Diagnosis  diagnosis = new Diagnosis();
        diagnosis.setDescription("TestDescription");
        diagnosis.setTitle("TestTitle");
        PageRequest pageRequest = new PageRequest(1, 10, Sort.Direction.ASC, "title", "description");
        Page<Diagnosis> found = genericRepository.findAll(pageRequest);
        assertEquals(found.iterator().next(), diagnosis);
    }

    //2 test,to check if found by id;
    @Test
    public void TestWhenFindByIdWithCorrectIdCalled() {
        Diagnosis  diagnosis = new Diagnosis();
        Long id = new Long(1);
        diagnosis.setDescription("TestDescription");
        diagnosis.setTitle("TestTitle");
        diagnosis.setId(id);
        Diagnosis found = service.findById(id);
        assertEquals(found, diagnosis);
    }

    //3 test, that wrong found
    @Test
    public void TestWhenFindByIdNonExistingIdCalled() {
        Diagnosis  diagnosis = new Diagnosis();
        Long id = new Long(2);
        diagnosis.setDescription("TestDescription");
        diagnosis.setTitle("TestTitle");
        diagnosis.setId(id);
        Diagnosis found = service.findById(id);
        assertNotEquals(found, diagnosis);
    }


    //4 test, find by title
    @Test
    public void TestWhenFindByTitleWithExistingTitleCalled() {
        Diagnosis  diagnosis = new Diagnosis();
        Long id = new Long(1);
        String title= "TestTitle";
        diagnosis.setDescription("TestDescription");
        diagnosis.setTitle(title);
        diagnosis.setId(id);
        Diagnosis found = service.findByTitle(title);
        assertEquals(found, diagnosis);
    }

    //5
    @Test
    public void TestWhenCorrectCreateCalled() {
        Diagnosis  diagnosis = new Diagnosis();
        Long id = new Long(1);
        String title= "TestTitle";
        diagnosis.setDescription("TestDescription");
        diagnosis.setTitle(title);
        diagnosis.setId(id);
        Diagnosis found = service.create(diagnosis);
        assertEquals(found, diagnosis);
    }

    @Test
    public void TestWhenCorrectUpdateCalled() {
        Diagnosis  diagnosis = new Diagnosis();
        Long id = new Long(1);
        String title= "TestTitle2";
        diagnosis.setDescription("TestDescription2");
        diagnosis.setTitle(title);
        diagnosis.setId(id);
        Diagnosis found = service.update(1L,diagnosis);
        assertEquals(found, diagnosis);
    }

    //**************************************************
    //***************Tests to catch exceptions left*****
    //**************************************************

}